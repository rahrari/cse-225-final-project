// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_fst_c.h"
#include "Vtestbench__Syms.h"


VL_ATTR_COLD void Vtestbench___024root__trace_init_sub__TOP__0(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_init_sub__TOP__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->pushNamePrefix("testbench ");
    tracep->declBus(c+49,"iterations_lp",-1, FST_VD_IMPLICIT,FST_VT_VCD_PARAMETER, false,-1, 31,0);
    tracep->declBus(c+41,"reset_done",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+42,"clk_i",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+8,"reset_i",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+1,"reset_button",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+2,"start_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+3,"p1_press_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+4,"p2_press_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+5,"five_sec_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+6,"ten_sec_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+7,"no_input",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+15,"p1_wins_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+16,"p2_wins_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+43,"error_p1_wins_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+44,"correct_p1_wins_o",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+9,"correct_p1_wins_n",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+45,"error_p2_wins_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+46,"correct_p2_wins_o",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+10,"correct_p2_wins_n",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+47,"itervar",-1, FST_VD_IMPLICIT,FST_VT_SV_INT, false,-1, 31,0);
    tracep->pushNamePrefix("cg ");
    tracep->declBus(c+50,"cycle_time_p",-1, FST_VD_IMPLICIT,FST_VT_VCD_PARAMETER, false,-1, 31,0);
    tracep->declBit(c+42,"clk_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("dut ");
    tracep->declBus(c+42,"clk_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+8,"reset_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+1,"quit_button_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+2,"start_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+3,"p1_press_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+4,"p2_press_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+5,"five_sec_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+6,"ten_sec_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+15,"p1_wins_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+16,"p2_wins_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+15,"p1_w_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+16,"p2_w_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+17,"total_games",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 2,0);
    {
        const char* __VenumItemNames[]
        = {"IDLE", "PLAY", "WIN"};
        const char* __VenumItemValues[]
        = {"0", "1", "10"};
        tracep->declDTypeEnum(1, "top.__typeimpenum0", 3, 2, __VenumItemNames, __VenumItemValues);
    }
    tracep->declBus(c+51,"state_r",1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 1,0);
    tracep->declBus(c+18,"curr_state_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 1,0);
    tracep->declBus(c+19,"num_p1_wins",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 1,0);
    tracep->declBus(c+20,"num_p2_wins",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 1,0);
    tracep->declBus(c+21,"target_num",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 2,0);
    tracep->declBus(c+22,"gs_idle_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+23,"gs_play_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+24,"gs_win_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+25,"gs_no_win_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+26,"ps_nopl_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+27,"ps_flas_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+28,"ps_play_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+29,"ps_win_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+30,"ps_lose_o_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+31,"data_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 4,0);
    tracep->declBus(c+48,"win_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+32,"d1_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+33,"w_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+34,"d2_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->pushNamePrefix("game_states_inst ");
    tracep->declBus(c+42,"clk_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+8,"reset_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+2,"start_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+35,"three_played_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+34,"w_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+1,"quit_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+22,"idle_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+23,"play_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+24,"win_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+25,"no_win_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    {
        const char* __VenumItemNames[]
        = {"IDLE", "PLAY", "WINS", "NOWS"};
        const char* __VenumItemValues[]
        = {"0", "1", "10", "11"};
        tracep->declDTypeEnum(2, "game_states.__typeimpenum2", 4, 2, __VenumItemNames, __VenumItemValues);
    }
    tracep->declBus(c+52,"state_r",2, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 1,0);
    tracep->declBus(c+36,"curr_state_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 1,0);
    tracep->declBus(c+22,"idle_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+23,"play_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+24,"win_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+25,"no_win_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("lfsr_inst ");
    tracep->declBus(c+42,"clk_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+8,"reset_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+31,"data_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 4,0);
    tracep->declBus(c+37,"data_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 3,0);
    tracep->declBus(c+38,"first_bit_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("play_states_inst ");
    tracep->declBus(c+42,"clk_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+8,"reset_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+23,"in_play_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+5,"five_sec_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+6,"ten_sec_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+32,"win_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+1,"quit_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+26,"nopl_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+27,"flas_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+28,"play_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+29,"wins_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+30,"lose_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    {
        const char* __VenumItemNames[]
        = {"NOPL", "FLAS", "PLAY", "WINS", "LOSE"};
        const char* __VenumItemValues[]
        = {"0", "1", "10", "11", "100"};
        tracep->declDTypeEnum(3, "play_states.__typeimpenum1", 5, 3, __VenumItemNames, __VenumItemValues);
    }
    tracep->declBus(c+53,"state_r",3, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 2,0);
    tracep->declBus(c+39,"curr_state_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 2,0);
    tracep->declBus(c+26,"nopl_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+27,"flas_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+28,"play_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+29,"wins_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+30,"lose_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+40,"games_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 2,0);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("rg ");
    tracep->declBus(c+54,"num_clocks_p",-1, FST_VD_IMPLICIT,FST_VT_VCD_PARAMETER, false,-1, 31,0);
    tracep->declBus(c+54,"reset_cycles_lo_p",-1, FST_VD_IMPLICIT,FST_VT_VCD_PARAMETER, false,-1, 31,0);
    tracep->declBus(c+50,"reset_cycles_hi_p",-1, FST_VD_IMPLICIT,FST_VT_VCD_PARAMETER, false,-1, 31,0);
    tracep->declBus(c+42,"clk_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBit(c+8,"async_reset_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1);
    tracep->declBus(c+11,"ctr_lo_r",-1, FST_VD_IMPLICIT,FST_VT_SV_BIT, false,-1, 0,0);
    tracep->declBus(c+12,"ctr_hi_r",-1, FST_VD_IMPLICIT,FST_VT_SV_BIT, false,-1, 3,0);
    tracep->declBus(c+13,"phase_lo_r",-1, FST_VD_IMPLICIT,FST_VT_SV_BIT, false,-1, 0,0);
    tracep->declBus(c+14,"phase_hi_r",-1, FST_VD_IMPLICIT,FST_VT_SV_BIT, false,-1, 0,0);
    tracep->declBit(c+13,"in_phase_1",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1);
    tracep->declBit(c+14,"in_phase_2",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1);
    tracep->popNamePrefix(2);
}

VL_ATTR_COLD void Vtestbench___024root__trace_init_top(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_init_top\n"); );
    // Body
    Vtestbench___024root__trace_init_sub__TOP__0(vlSelf, tracep);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_top_0(void* voidSelf, VerilatedFst::Buffer* bufp);
void Vtestbench___024root__trace_chg_top_0(void* voidSelf, VerilatedFst::Buffer* bufp);
void Vtestbench___024root__trace_cleanup(void* voidSelf, VerilatedFst* /*unused*/);

VL_ATTR_COLD void Vtestbench___024root__trace_register(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_register\n"); );
    // Body
    tracep->addFullCb(&Vtestbench___024root__trace_full_top_0, vlSelf);
    tracep->addChgCb(&Vtestbench___024root__trace_chg_top_0, vlSelf);
    tracep->addCleanupCb(&Vtestbench___024root__trace_cleanup, vlSelf);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp);

VL_ATTR_COLD void Vtestbench___024root__trace_full_top_0(void* voidSelf, VerilatedFst::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_full_top_0\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    Vtestbench___024root__trace_full_sub_0((&vlSymsp->TOP), bufp);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_full_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode);
    // Body
    bufp->fullBit(oldp+1,(vlSelf->testbench__DOT__reset_button));
    bufp->fullBit(oldp+2,(vlSelf->testbench__DOT__start_i));
    bufp->fullBit(oldp+3,(vlSelf->testbench__DOT__p1_press_i));
    bufp->fullBit(oldp+4,(vlSelf->testbench__DOT__p2_press_i));
    bufp->fullBit(oldp+5,(vlSelf->testbench__DOT__five_sec_i));
    bufp->fullBit(oldp+6,(vlSelf->testbench__DOT__ten_sec_i));
    bufp->fullBit(oldp+7,(vlSelf->testbench__DOT__no_input));
    bufp->fullBit(oldp+8,(vlSelf->testbench__DOT__reset_i));
    bufp->fullBit(oldp+9,(vlSelf->testbench__DOT__correct_p1_wins_n));
    bufp->fullBit(oldp+10,(vlSelf->testbench__DOT__correct_p2_wins_n));
    bufp->fullBit(oldp+11,(vlSelf->testbench__DOT__rg__DOT__ctr_lo_r));
    bufp->fullCData(oldp+12,(vlSelf->testbench__DOT__rg__DOT__ctr_hi_r),4);
    bufp->fullBit(oldp+13,(vlSelf->testbench__DOT__rg__DOT__ctr_lo_r));
    bufp->fullBit(oldp+14,((0xaU == (IData)(vlSelf->testbench__DOT__rg__DOT__ctr_hi_r))));
    bufp->fullBit(oldp+15,(vlSelf->testbench__DOT__dut__DOT__p1_w_l));
    bufp->fullBit(oldp+16,(vlSelf->testbench__DOT__dut__DOT__p2_w_l));
    bufp->fullCData(oldp+17,(vlSelf->testbench__DOT__dut__DOT__total_games),3);
    bufp->fullCData(oldp+18,(vlSelf->testbench__DOT__dut__DOT__curr_state_l),2);
    bufp->fullCData(oldp+19,(vlSelf->testbench__DOT__dut__DOT__num_p1_wins),2);
    bufp->fullCData(oldp+20,(vlSelf->testbench__DOT__dut__DOT__num_p2_wins),2);
    bufp->fullCData(oldp+21,(vlSelf->testbench__DOT__dut__DOT__target_num),3);
    bufp->fullBit(oldp+22,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__idle_l));
    bufp->fullBit(oldp+23,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__play_l));
    bufp->fullBit(oldp+24,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__win_l));
    bufp->fullBit(oldp+25,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__no_win_l));
    bufp->fullBit(oldp+26,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l));
    bufp->fullBit(oldp+27,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l));
    bufp->fullBit(oldp+28,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l));
    bufp->fullBit(oldp+29,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l));
    bufp->fullBit(oldp+30,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l));
    bufp->fullCData(oldp+31,(vlSelf->testbench__DOT__dut__DOT__data_l),5);
    bufp->fullBit(oldp+32,(vlSelf->testbench__DOT__dut__DOT__d1_l));
    bufp->fullBit(oldp+33,(((((IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l) 
                              > (IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l)) 
                             | ((IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l) 
                                > (IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l))) 
                            & (0U == (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)))));
    bufp->fullBit(oldp+34,(vlSelf->testbench__DOT__dut__DOT__d2_l));
    bufp->fullBit(oldp+35,((0U == (IData)(vlSelf->testbench__DOT__dut__DOT__total_games))));
    bufp->fullCData(oldp+36,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__curr_state_l),2);
    bufp->fullCData(oldp+37,(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l),4);
    bufp->fullBit(oldp+38,(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l));
    bufp->fullCData(oldp+39,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l),3);
    bufp->fullCData(oldp+40,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__games_l),3);
    bufp->fullBit(oldp+41,(vlSelf->testbench__DOT__reset_done));
    bufp->fullBit(oldp+42,(vlSelf->testbench__DOT__clk_i));
    bufp->fullBit(oldp+43,((((IData)(vlSelf->testbench__DOT__correct_p1_wins_o) 
                             != (IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l)) 
                            & VL_GTS_III(32, 0x49U, vlSelf->testbench__DOT__itervar))));
    bufp->fullBit(oldp+44,(vlSelf->testbench__DOT__correct_p1_wins_o));
    bufp->fullBit(oldp+45,((((IData)(vlSelf->testbench__DOT__correct_p2_wins_o) 
                             != (IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l)) 
                            & VL_GTS_III(32, 0x49U, vlSelf->testbench__DOT__itervar))));
    bufp->fullBit(oldp+46,(vlSelf->testbench__DOT__correct_p2_wins_o));
    bufp->fullIData(oldp+47,(vlSelf->testbench__DOT__itervar),32);
    bufp->fullBit(oldp+48,(((((~ (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)) 
                              & (IData)(vlSelf->testbench__DOT__p1_press_i)) 
                             | ((IData)(vlSelf->testbench__DOT__p2_press_i) 
                                & (IData)(vlSelf->testbench__DOT__dut__DOT__total_games))) 
                            & (((6U & ((IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l) 
                                       << 1U)) | (IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l)) 
                               == (IData)(vlSelf->testbench__DOT__dut__DOT__target_num)))));
    bufp->fullIData(oldp+49,(0x49U),32);
    bufp->fullIData(oldp+50,(0xaU),32);
    bufp->fullCData(oldp+51,(vlSelf->testbench__DOT__dut__DOT__state_r),2);
    bufp->fullCData(oldp+52,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__state_r),2);
    bufp->fullCData(oldp+53,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__state_r),3);
    bufp->fullIData(oldp+54,(1U),32);
}
