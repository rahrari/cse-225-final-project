// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_fst_c.h"
#include "Vtestbench__Syms.h"


void Vtestbench___024root__trace_chg_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp);

void Vtestbench___024root__trace_chg_top_0(void* voidSelf, VerilatedFst::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_chg_top_0\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    Vtestbench___024root__trace_chg_sub_0((&vlSymsp->TOP), bufp);
}

void Vtestbench___024root__trace_chg_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_chg_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode + 1);
    // Body
    if (VL_UNLIKELY(((vlSelf->__Vm_traceActivity[1U] 
                      | vlSelf->__Vm_traceActivity[2U]) 
                     | vlSelf->__Vm_traceActivity[3U]))) {
        bufp->chgBit(oldp+0,(vlSelf->testbench__DOT__reset_button));
        bufp->chgBit(oldp+1,(vlSelf->testbench__DOT__start_i));
        bufp->chgBit(oldp+2,(vlSelf->testbench__DOT__p1_press_i));
        bufp->chgBit(oldp+3,(vlSelf->testbench__DOT__p2_press_i));
        bufp->chgBit(oldp+4,(vlSelf->testbench__DOT__five_sec_i));
        bufp->chgBit(oldp+5,(vlSelf->testbench__DOT__ten_sec_i));
        bufp->chgBit(oldp+6,(vlSelf->testbench__DOT__no_input));
    }
    if (VL_UNLIKELY(vlSelf->__Vm_traceActivity[3U])) {
        bufp->chgBit(oldp+7,(vlSelf->testbench__DOT__reset_i));
        bufp->chgBit(oldp+8,(vlSelf->testbench__DOT__correct_p1_wins_n));
        bufp->chgBit(oldp+9,(vlSelf->testbench__DOT__correct_p2_wins_n));
        bufp->chgBit(oldp+10,(vlSelf->testbench__DOT__rg__DOT__ctr_lo_r));
        bufp->chgCData(oldp+11,(vlSelf->testbench__DOT__rg__DOT__ctr_hi_r),4);
        bufp->chgBit(oldp+12,(vlSelf->testbench__DOT__rg__DOT__ctr_lo_r));
        bufp->chgBit(oldp+13,((0xaU == (IData)(vlSelf->testbench__DOT__rg__DOT__ctr_hi_r))));
    }
    if (VL_UNLIKELY(vlSelf->__Vm_traceActivity[4U])) {
        bufp->chgBit(oldp+14,(vlSelf->testbench__DOT__dut__DOT__p1_w_l));
        bufp->chgBit(oldp+15,(vlSelf->testbench__DOT__dut__DOT__p2_w_l));
        bufp->chgCData(oldp+16,(vlSelf->testbench__DOT__dut__DOT__total_games),3);
        bufp->chgCData(oldp+17,(vlSelf->testbench__DOT__dut__DOT__curr_state_l),2);
        bufp->chgCData(oldp+18,(vlSelf->testbench__DOT__dut__DOT__num_p1_wins),2);
        bufp->chgCData(oldp+19,(vlSelf->testbench__DOT__dut__DOT__num_p2_wins),2);
        bufp->chgCData(oldp+20,(vlSelf->testbench__DOT__dut__DOT__target_num),3);
        bufp->chgBit(oldp+21,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__idle_l));
        bufp->chgBit(oldp+22,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__play_l));
        bufp->chgBit(oldp+23,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__win_l));
        bufp->chgBit(oldp+24,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__no_win_l));
        bufp->chgBit(oldp+25,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l));
        bufp->chgBit(oldp+26,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l));
        bufp->chgBit(oldp+27,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l));
        bufp->chgBit(oldp+28,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l));
        bufp->chgBit(oldp+29,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l));
        bufp->chgCData(oldp+30,(vlSelf->testbench__DOT__dut__DOT__data_l),5);
        bufp->chgBit(oldp+31,(vlSelf->testbench__DOT__dut__DOT__d1_l));
        bufp->chgBit(oldp+32,(((((IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l) 
                                 > (IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l)) 
                                | ((IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l) 
                                   > (IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l))) 
                               & (0U == (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)))));
        bufp->chgBit(oldp+33,(vlSelf->testbench__DOT__dut__DOT__d2_l));
        bufp->chgBit(oldp+34,((0U == (IData)(vlSelf->testbench__DOT__dut__DOT__total_games))));
        bufp->chgCData(oldp+35,(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__curr_state_l),2);
        bufp->chgCData(oldp+36,(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l),4);
        bufp->chgBit(oldp+37,(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l));
        bufp->chgCData(oldp+38,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l),3);
        bufp->chgCData(oldp+39,(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__games_l),3);
    }
    bufp->chgBit(oldp+40,(vlSelf->testbench__DOT__reset_done));
    bufp->chgBit(oldp+41,(vlSelf->testbench__DOT__clk_i));
    bufp->chgBit(oldp+42,((((IData)(vlSelf->testbench__DOT__correct_p1_wins_o) 
                            != (IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l)) 
                           & VL_GTS_III(32, 0x49U, vlSelf->testbench__DOT__itervar))));
    bufp->chgBit(oldp+43,(vlSelf->testbench__DOT__correct_p1_wins_o));
    bufp->chgBit(oldp+44,((((IData)(vlSelf->testbench__DOT__correct_p2_wins_o) 
                            != (IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l)) 
                           & VL_GTS_III(32, 0x49U, vlSelf->testbench__DOT__itervar))));
    bufp->chgBit(oldp+45,(vlSelf->testbench__DOT__correct_p2_wins_o));
    bufp->chgIData(oldp+46,(vlSelf->testbench__DOT__itervar),32);
    bufp->chgBit(oldp+47,(((((~ (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)) 
                             & (IData)(vlSelf->testbench__DOT__p1_press_i)) 
                            | ((IData)(vlSelf->testbench__DOT__p2_press_i) 
                               & (IData)(vlSelf->testbench__DOT__dut__DOT__total_games))) 
                           & (((6U & ((IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l) 
                                      << 1U)) | (IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l)) 
                              == (IData)(vlSelf->testbench__DOT__dut__DOT__target_num)))));
}

void Vtestbench___024root__trace_cleanup(void* voidSelf, VerilatedFst* /*unused*/) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_cleanup\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    vlSymsp->__Vm_activity = false;
    vlSymsp->TOP.__Vm_traceActivity[0U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[1U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[2U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[3U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[4U] = 0U;
}
