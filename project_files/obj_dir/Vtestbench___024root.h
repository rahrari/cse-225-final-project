// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vtestbench.h for the primary calling header

#ifndef VERILATED_VTESTBENCH___024ROOT_H_
#define VERILATED_VTESTBENCH___024ROOT_H_  // guard

#include "verilated.h"
#include "verilated_timing.h"

class Vtestbench__Syms;

class Vtestbench___024root final : public VerilatedModule {
  public:

    // DESIGN SPECIFIC STATE
    CData/*0:0*/ testbench__DOT__clk_i;
    CData/*0:0*/ testbench__DOT__reset_i;
    CData/*0:0*/ testbench__DOT__reset_done;
    CData/*0:0*/ testbench__DOT__reset_button;
    CData/*0:0*/ testbench__DOT__start_i;
    CData/*0:0*/ testbench__DOT__p1_press_i;
    CData/*0:0*/ testbench__DOT__p2_press_i;
    CData/*0:0*/ testbench__DOT__five_sec_i;
    CData/*0:0*/ testbench__DOT__ten_sec_i;
    CData/*0:0*/ testbench__DOT__no_input;
    CData/*0:0*/ testbench__DOT__correct_p1_wins_o;
    CData/*0:0*/ testbench__DOT__correct_p1_wins_n;
    CData/*0:0*/ testbench__DOT__correct_p2_wins_o;
    CData/*0:0*/ testbench__DOT__correct_p2_wins_n;
    CData/*0:0*/ testbench__DOT__rg__DOT__ctr_lo_r;
    CData/*3:0*/ testbench__DOT__rg__DOT__ctr_hi_r;
    CData/*0:0*/ testbench__DOT__rg__DOT__phase_lo_r;
    CData/*0:0*/ testbench__DOT__rg__DOT__phase_hi_r;
    CData/*0:0*/ testbench__DOT__dut__DOT__p1_w_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__p2_w_l;
    CData/*2:0*/ testbench__DOT__dut__DOT__total_games;
    CData/*1:0*/ testbench__DOT__dut__DOT__state_r;
    CData/*1:0*/ testbench__DOT__dut__DOT__curr_state_l;
    CData/*1:0*/ testbench__DOT__dut__DOT__num_p1_wins;
    CData/*1:0*/ testbench__DOT__dut__DOT__num_p2_wins;
    CData/*2:0*/ testbench__DOT__dut__DOT__target_num;
    CData/*4:0*/ testbench__DOT__dut__DOT__data_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__d1_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__d2_l;
    CData/*3:0*/ testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l;
    CData/*2:0*/ testbench__DOT__dut__DOT__play_states_inst__DOT__state_r;
    CData/*2:0*/ testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__play_states_inst__DOT__play_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l;
    CData/*2:0*/ testbench__DOT__dut__DOT__play_states_inst__DOT__games_l;
    CData/*1:0*/ testbench__DOT__dut__DOT__game_states_inst__DOT__state_r;
    CData/*1:0*/ testbench__DOT__dut__DOT__game_states_inst__DOT__curr_state_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__game_states_inst__DOT__idle_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__game_states_inst__DOT__play_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__game_states_inst__DOT__win_l;
    CData/*0:0*/ testbench__DOT__dut__DOT__game_states_inst__DOT__no_win_l;
    CData/*0:0*/ __Vtrigrprev__TOP__testbench__DOT__clk_i;
    CData/*0:0*/ __Vtrigrprev__TOP__testbench__DOT__reset_i;
    CData/*0:0*/ __VactContinue;
    IData/*31:0*/ testbench__DOT__itervar;
    IData/*31:0*/ __VstlIterCount;
    IData/*31:0*/ __VactIterCount;
    VlUnpacked<SData/*8:0*/, 74> testbench__DOT__test_symbols;
    VlUnpacked<CData/*0:0*/, 5> __Vm_traceActivity;
    VlTriggerScheduler __VtrigSched_h382f1e33__0;
    VlTriggerScheduler __VtrigSched_hef748430__0;
    VlDelayScheduler __VdlySched;
    VlTriggerVec<1> __VstlTriggered;
    VlTriggerVec<5> __VactTriggered;
    VlTriggerVec<5> __VnbaTriggered;

    // INTERNAL VARIABLES
    Vtestbench__Syms* const vlSymsp;

    // CONSTRUCTORS
    Vtestbench___024root(Vtestbench__Syms* symsp, const char* name);
    ~Vtestbench___024root();
    VL_UNCOPYABLE(Vtestbench___024root);

    // INTERNAL METHODS
    void __Vconfigure(bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
