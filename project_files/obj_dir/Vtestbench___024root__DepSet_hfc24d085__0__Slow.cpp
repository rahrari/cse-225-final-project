// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtestbench.h for the primary calling header

#include "verilated.h"

#include "Vtestbench__Syms.h"
#include "Vtestbench___024root.h"

VL_ATTR_COLD void Vtestbench___024root___eval_initial__TOP(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial__TOP\n"); );
    // Body
    vlSelf->testbench__DOT__test_symbols[0U] = 1U;
    vlSelf->testbench__DOT__test_symbols[1U] = 0x10U;
    vlSelf->testbench__DOT__test_symbols[2U] = 0U;
    vlSelf->testbench__DOT__test_symbols[3U] = 0x80U;
    vlSelf->testbench__DOT__test_symbols[4U] = 0x20U;
    vlSelf->testbench__DOT__test_symbols[5U] = 0U;
    vlSelf->testbench__DOT__test_symbols[6U] = 0U;
    vlSelf->testbench__DOT__test_symbols[7U] = 0x80U;
    vlSelf->testbench__DOT__test_symbols[8U] = 0U;
    vlSelf->testbench__DOT__test_symbols[9U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0xaU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0xbU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0xcU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0xdU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0xeU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0xfU] = 0x40U;
    vlSelf->testbench__DOT__test_symbols[0x10U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x11U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x12U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x13U] = 0x80U;
    vlSelf->testbench__DOT__test_symbols[0x14U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x15U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x16U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x17U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x18U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x19U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x1aU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x1bU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x1cU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x1dU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x1eU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x1fU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x20U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x21U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x22U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x23U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x24U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x25U] = 0x20U;
    vlSelf->testbench__DOT__test_symbols[0x26U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x27U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x28U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x29U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x2aU] = 0x80U;
    vlSelf->testbench__DOT__test_symbols[0x2bU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x2cU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x2dU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x2eU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x2fU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x30U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x31U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x32U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x33U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x34U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x35U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x36U] = 0x40U;
    vlSelf->testbench__DOT__test_symbols[0x37U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x38U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x39U] = 0x80U;
    vlSelf->testbench__DOT__test_symbols[0x3aU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x3bU] = 0x20U;
    vlSelf->testbench__DOT__test_symbols[0x3cU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x3dU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x3eU] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x3fU] = 0x80U;
    vlSelf->testbench__DOT__test_symbols[0x40U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x41U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x42U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x43U] = 0x100U;
    vlSelf->testbench__DOT__test_symbols[0x44U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x45U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x46U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x47U] = 4U;
    vlSelf->testbench__DOT__test_symbols[0x48U] = 0U;
    vlSelf->testbench__DOT__test_symbols[0x49U] = 0U;
    VL_WRITEF("%Ntestbench.cg with cycle_time_p          10\n",
              vlSymsp->name());
}

#ifdef VL_DEBUG
VL_ATTR_COLD void Vtestbench___024root___dump_triggers__stl(Vtestbench___024root* vlSelf);
#endif  // VL_DEBUG

VL_ATTR_COLD void Vtestbench___024root___eval_triggers__stl(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_triggers__stl\n"); );
    // Body
    vlSelf->__VstlTriggered.at(0U) = (0U == vlSelf->__VstlIterCount);
#ifdef VL_DEBUG
    if (VL_UNLIKELY(vlSymsp->_vm_contextp__->debug())) {
        Vtestbench___024root___dump_triggers__stl(vlSelf);
    }
#endif
}
