// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtestbench.h for the primary calling header

#include "verilated.h"

#include "Vtestbench___024root.h"

VL_ATTR_COLD void Vtestbench___024root___eval_initial__TOP(Vtestbench___024root* vlSelf);
VlCoroutine Vtestbench___024root___eval_initial__TOP__0(Vtestbench___024root* vlSelf);
VlCoroutine Vtestbench___024root___eval_initial__TOP__1(Vtestbench___024root* vlSelf);

void Vtestbench___024root___eval_initial(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial\n"); );
    // Body
    Vtestbench___024root___eval_initial__TOP(vlSelf);
    vlSelf->__Vm_traceActivity[1U] = 1U;
    Vtestbench___024root___eval_initial__TOP__0(vlSelf);
    Vtestbench___024root___eval_initial__TOP__1(vlSelf);
    vlSelf->__Vtrigrprev__TOP__testbench__DOT__clk_i 
        = vlSelf->testbench__DOT__clk_i;
    vlSelf->__Vtrigrprev__TOP__testbench__DOT__reset_i 
        = vlSelf->testbench__DOT__reset_i;
}

VL_INLINE_OPT VlCoroutine Vtestbench___024root___eval_initial__TOP__1(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial__TOP__1\n"); );
    // Body
    while (1U) {
        co_await vlSelf->__VdlySched.delay(0x1388U, 
                                           "../provided_modules/nonsynth_clock_gen.sv", 
                                           11);
        vlSelf->testbench__DOT__clk_i = (1U & (~ (IData)(vlSelf->testbench__DOT__clk_i)));
    }
}

void Vtestbench___024root___eval_act(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_act\n"); );
}

VL_INLINE_OPT void Vtestbench___024root___nba_sequent__TOP__0(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___nba_sequent__TOP__0\n"); );
    // Body
    VL_WRITEF("__________ ___________  _______________________________\n\\______   \\\\_   _____/ /   _____/\\_   _____/\\__    ___/\n |       _/ |    __)_  \\_____  \\  |    __)_   |    |   \n |    |   \\ |        \\ /        \\ |        \\  |    |  1->0 time = %10#\n |____|_  //_______  //_______  //_______  /  |____|   \n ASYNC  \\/         \\/         \\/         \\/            \n",
              32,(IData)(VL_TIME_UNITED_Q(1000)));
}

VL_INLINE_OPT void Vtestbench___024root___nba_sequent__TOP__1(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___nba_sequent__TOP__1\n"); );
    // Body
    VL_WRITEF("__________ ___________  _______________________________\n\\______   \\\\_   _____/ /   _____/\\_   _____/\\__    ___/\n |       _/ |    __)_  \\_____  \\  |    __)_   |    |   \n |    |   \\ |        \\ /        \\ |        \\  |    |  0->1 time = %10#\n |____|_  //_______  //_______  //_______  /  |____|   \n ASYNC  \\/         \\/         \\/         \\/            \n",
              32,(IData)(VL_TIME_UNITED_Q(1000)));
}

VL_INLINE_OPT void Vtestbench___024root___nba_sequent__TOP__2(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___nba_sequent__TOP__2\n"); );
    // Init
    CData/*0:0*/ __Vdly__testbench__DOT__rg__DOT__ctr_lo_r;
    __Vdly__testbench__DOT__rg__DOT__ctr_lo_r = 0;
    // Body
    __Vdly__testbench__DOT__rg__DOT__ctr_lo_r = vlSelf->testbench__DOT__rg__DOT__ctr_lo_r;
    if (VL_UNLIKELY((((~ (IData)(vlSelf->testbench__DOT__reset_i)) 
                      & (IData)(vlSelf->testbench__DOT__reset_done)) 
                     & (((IData)(vlSelf->testbench__DOT__correct_p1_wins_o) 
                         != (IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l)) 
                        & VL_GTS_III(32, 0x49U, vlSelf->testbench__DOT__itervar))))) {
        VL_WRITEF("\033[0;31mError!\033[0m: p1_wins_o should be %b, but got %b\n",
                  1,vlSelf->testbench__DOT__correct_p1_wins_o,
                  1,(IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l));
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->testbench__DOT__reset_i)) 
                      & (IData)(vlSelf->testbench__DOT__reset_done)) 
                     & (((IData)(vlSelf->testbench__DOT__correct_p2_wins_o) 
                         != (IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l)) 
                        & VL_GTS_III(32, 0x49U, vlSelf->testbench__DOT__itervar))))) {
        VL_WRITEF("\033[0;31mError!\033[0m: p2_wins_o should be %b, but got %b\n",
                  1,vlSelf->testbench__DOT__correct_p2_wins_o,
                  1,(IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l));
    }
    if (VL_UNLIKELY(VL_LTES_III(32, 0x49U, vlSelf->testbench__DOT__itervar))) {
        VL_FINISH_MT("testbench.sv", 190, "");
    }
    vlSelf->testbench__DOT__p1_press_i = (1U & (((0x49U 
                                                  >= 
                                                  (0x7fU 
                                                   & vlSelf->testbench__DOT__itervar))
                                                  ? 
                                                 vlSelf->testbench__DOT__test_symbols
                                                 [(0x7fU 
                                                   & vlSelf->testbench__DOT__itervar)]
                                                  : 0U) 
                                                >> 5U));
    vlSelf->testbench__DOT__p2_press_i = (1U & (((0x49U 
                                                  >= 
                                                  (0x7fU 
                                                   & vlSelf->testbench__DOT__itervar))
                                                  ? 
                                                 vlSelf->testbench__DOT__test_symbols
                                                 [(0x7fU 
                                                   & vlSelf->testbench__DOT__itervar)]
                                                  : 0U) 
                                                >> 6U));
    vlSelf->testbench__DOT__start_i = (1U & (((0x49U 
                                               >= (0x7fU 
                                                   & vlSelf->testbench__DOT__itervar))
                                               ? vlSelf->testbench__DOT__test_symbols
                                              [(0x7fU 
                                                & vlSelf->testbench__DOT__itervar)]
                                               : 0U) 
                                             >> 4U));
    vlSelf->testbench__DOT__ten_sec_i = (1U & (((0x49U 
                                                 >= 
                                                 (0x7fU 
                                                  & vlSelf->testbench__DOT__itervar))
                                                 ? 
                                                vlSelf->testbench__DOT__test_symbols
                                                [(0x7fU 
                                                  & vlSelf->testbench__DOT__itervar)]
                                                 : 0U) 
                                               >> 8U));
    vlSelf->testbench__DOT__five_sec_i = (1U & (((0x49U 
                                                  >= 
                                                  (0x7fU 
                                                   & vlSelf->testbench__DOT__itervar))
                                                  ? 
                                                 vlSelf->testbench__DOT__test_symbols
                                                 [(0x7fU 
                                                   & vlSelf->testbench__DOT__itervar)]
                                                  : 0U) 
                                                >> 7U));
    vlSelf->testbench__DOT__correct_p2_wins_n = (1U 
                                                 & (((0x49U 
                                                      >= 
                                                      (0x7fU 
                                                       & vlSelf->testbench__DOT__itervar))
                                                      ? 
                                                     vlSelf->testbench__DOT__test_symbols
                                                     [
                                                     (0x7fU 
                                                      & vlSelf->testbench__DOT__itervar)]
                                                      : 0U) 
                                                    >> 3U));
    vlSelf->testbench__DOT__correct_p1_wins_n = (1U 
                                                 & (((0x49U 
                                                      >= 
                                                      (0x7fU 
                                                       & vlSelf->testbench__DOT__itervar))
                                                      ? 
                                                     vlSelf->testbench__DOT__test_symbols
                                                     [
                                                     (0x7fU 
                                                      & vlSelf->testbench__DOT__itervar)]
                                                      : 0U) 
                                                    >> 2U));
    vlSelf->testbench__DOT__reset_button = (1U & ((
                                                   (0x49U 
                                                    >= 
                                                    (0x7fU 
                                                     & vlSelf->testbench__DOT__itervar))
                                                    ? 
                                                   vlSelf->testbench__DOT__test_symbols
                                                   [
                                                   (0x7fU 
                                                    & vlSelf->testbench__DOT__itervar)]
                                                    : 0U) 
                                                  >> 1U));
    vlSelf->testbench__DOT__no_input = ((0x49U >= (0x7fU 
                                                   & vlSelf->testbench__DOT__itervar))
                                         ? (1U & vlSelf->testbench__DOT__test_symbols
                                            [(0x7fU 
                                              & vlSelf->testbench__DOT__itervar)])
                                         : 0U);
    if ((1U & (~ (IData)(vlSelf->testbench__DOT__rg__DOT__phase_lo_r)))) {
        __Vdly__testbench__DOT__rg__DOT__ctr_lo_r = 
            (1U & ((IData)(1U) + (IData)(vlSelf->testbench__DOT__rg__DOT__ctr_lo_r)));
    }
    if (vlSelf->testbench__DOT__rg__DOT__ctr_lo_r) {
        if ((1U & (~ (IData)(vlSelf->testbench__DOT__rg__DOT__phase_hi_r)))) {
            vlSelf->testbench__DOT__rg__DOT__ctr_hi_r 
                = (0xfU & ((IData)(vlSelf->testbench__DOT__rg__DOT__ctr_hi_r) 
                           + (IData)(vlSelf->testbench__DOT__rg__DOT__ctr_lo_r)));
        }
    }
    vlSelf->testbench__DOT__rg__DOT__ctr_lo_r = __Vdly__testbench__DOT__rg__DOT__ctr_lo_r;
    vlSelf->testbench__DOT__rg__DOT__phase_lo_r = vlSelf->testbench__DOT__rg__DOT__ctr_lo_r;
    vlSelf->testbench__DOT__rg__DOT__phase_hi_r = (0xaU 
                                                   == (IData)(vlSelf->testbench__DOT__rg__DOT__ctr_hi_r));
    vlSelf->testbench__DOT__reset_i = ((IData)(vlSelf->testbench__DOT__rg__DOT__ctr_lo_r) 
                                       ^ (0xaU == (IData)(vlSelf->testbench__DOT__rg__DOT__ctr_hi_r)));
}

extern const VlUnpacked<CData/*1:0*/, 128> Vtestbench__ConstPool__TABLE_h8558e017_0;
extern const VlUnpacked<CData/*4:0*/, 128> Vtestbench__ConstPool__TABLE_h28f2b6de_0;
extern const VlUnpacked<CData/*0:0*/, 128> Vtestbench__ConstPool__TABLE_h4b211ce9_0;
extern const VlUnpacked<CData/*0:0*/, 128> Vtestbench__ConstPool__TABLE_hf60ba374_0;
extern const VlUnpacked<CData/*0:0*/, 128> Vtestbench__ConstPool__TABLE_ha498e8eb_0;
extern const VlUnpacked<CData/*0:0*/, 128> Vtestbench__ConstPool__TABLE_h71834f84_0;

VL_INLINE_OPT void Vtestbench___024root___nba_sequent__TOP__3(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___nba_sequent__TOP__3\n"); );
    // Init
    CData/*6:0*/ __Vtableidx1;
    __Vtableidx1 = 0;
    CData/*1:0*/ __Vdly__testbench__DOT__dut__DOT__curr_state_l;
    __Vdly__testbench__DOT__dut__DOT__curr_state_l = 0;
    CData/*1:0*/ __Vdly__testbench__DOT__dut__DOT__num_p1_wins;
    __Vdly__testbench__DOT__dut__DOT__num_p1_wins = 0;
    CData/*1:0*/ __Vdly__testbench__DOT__dut__DOT__num_p2_wins;
    __Vdly__testbench__DOT__dut__DOT__num_p2_wins = 0;
    CData/*2:0*/ __Vdly__testbench__DOT__dut__DOT__total_games;
    __Vdly__testbench__DOT__dut__DOT__total_games = 0;
    CData/*2:0*/ __Vdly__testbench__DOT__dut__DOT__target_num;
    __Vdly__testbench__DOT__dut__DOT__target_num = 0;
    CData/*0:0*/ __Vdly__testbench__DOT__dut__DOT__d2_l;
    __Vdly__testbench__DOT__dut__DOT__d2_l = 0;
    CData/*3:0*/ __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l;
    __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l = 0;
    CData/*0:0*/ __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l;
    __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l = 0;
    CData/*2:0*/ __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l;
    __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 0;
    CData/*2:0*/ __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l;
    __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l = 0;
    // Body
    __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l 
        = vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l;
    __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l 
        = vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l;
    __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l 
        = vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__games_l;
    __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l 
        = vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l;
    __Vdly__testbench__DOT__dut__DOT__target_num = vlSelf->testbench__DOT__dut__DOT__target_num;
    __Vdly__testbench__DOT__dut__DOT__total_games = vlSelf->testbench__DOT__dut__DOT__total_games;
    __Vdly__testbench__DOT__dut__DOT__num_p2_wins = vlSelf->testbench__DOT__dut__DOT__num_p2_wins;
    __Vdly__testbench__DOT__dut__DOT__num_p1_wins = vlSelf->testbench__DOT__dut__DOT__num_p1_wins;
    __Vdly__testbench__DOT__dut__DOT__curr_state_l 
        = vlSelf->testbench__DOT__dut__DOT__curr_state_l;
    __Vdly__testbench__DOT__dut__DOT__d2_l = vlSelf->testbench__DOT__dut__DOT__d2_l;
    if (vlSelf->testbench__DOT__reset_i) {
        __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l = 0U;
        __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l = 1U;
    } else {
        __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l 
            = (1U & ((IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l) 
                     ^ ((IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l) 
                        >> 3U)));
        __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l 
            = ((0xeU & ((IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l) 
                        << 1U)) | (IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l));
    }
    __Vdly__testbench__DOT__dut__DOT__d2_l = ((((IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l) 
                                                > (IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l)) 
                                               | ((IData)(vlSelf->testbench__DOT__dut__DOT__p2_w_l) 
                                                  > (IData)(vlSelf->testbench__DOT__dut__DOT__p1_w_l))) 
                                              & (0U 
                                                 == (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)));
    if (((IData)(vlSelf->testbench__DOT__reset_i) | (IData)(vlSelf->testbench__DOT__reset_button))) {
        __Vdly__testbench__DOT__dut__DOT__curr_state_l = 0U;
        vlSelf->testbench__DOT__dut__DOT__p1_w_l = 0U;
        vlSelf->testbench__DOT__dut__DOT__p2_w_l = 0U;
        __Vdly__testbench__DOT__dut__DOT__num_p1_wins = 0U;
        __Vdly__testbench__DOT__dut__DOT__num_p2_wins = 0U;
        __Vdly__testbench__DOT__dut__DOT__total_games = 6U;
        __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 0U;
        vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l = 1U;
        vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 0U;
        vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
        vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 0U;
        vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 0U;
        __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l = 5U;
    } else {
        if ((0U == (IData)(vlSelf->testbench__DOT__dut__DOT__curr_state_l))) {
            if (vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__play_l) {
                __Vdly__testbench__DOT__dut__DOT__curr_state_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__p1_w_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__p2_w_l = 0U;
                __Vdly__testbench__DOT__dut__DOT__num_p1_wins = 0U;
                __Vdly__testbench__DOT__dut__DOT__num_p2_wins = 0U;
                __Vdly__testbench__DOT__dut__DOT__target_num 
                    = (7U & (IData)(vlSelf->testbench__DOT__dut__DOT__data_l));
                __Vdly__testbench__DOT__dut__DOT__total_games = 6U;
            } else {
                __Vdly__testbench__DOT__dut__DOT__curr_state_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__p1_w_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__p2_w_l = 0U;
                __Vdly__testbench__DOT__dut__DOT__num_p1_wins = 0U;
                __Vdly__testbench__DOT__dut__DOT__num_p2_wins = 0U;
                __Vdly__testbench__DOT__dut__DOT__total_games = 6U;
            }
        } else if ((1U == (IData)(vlSelf->testbench__DOT__dut__DOT__curr_state_l))) {
            if ((0U == (IData)(vlSelf->testbench__DOT__dut__DOT__total_games))) {
                __Vdly__testbench__DOT__dut__DOT__curr_state_l 
                    = ((((IData)(vlSelf->testbench__DOT__dut__DOT__num_p1_wins) 
                         > (IData)(vlSelf->testbench__DOT__dut__DOT__num_p2_wins)) 
                        | ((IData)(vlSelf->testbench__DOT__dut__DOT__num_p1_wins) 
                           > (IData)(vlSelf->testbench__DOT__dut__DOT__num_p2_wins)))
                        ? 2U : 0U);
            } else {
                if (vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l) {
                    __Vdly__testbench__DOT__dut__DOT__curr_state_l = 1U;
                    __Vdly__testbench__DOT__dut__DOT__total_games 
                        = (7U & ((IData)(7U) + (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)));
                    __Vdly__testbench__DOT__dut__DOT__target_num 
                        = (7U & (IData)(vlSelf->testbench__DOT__dut__DOT__data_l));
                    if ((1U & (~ (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)))) {
                        __Vdly__testbench__DOT__dut__DOT__num_p1_wins 
                            = (3U & ((IData)(vlSelf->testbench__DOT__dut__DOT__num_p1_wins) 
                                     - (IData)(3U)));
                        __Vdly__testbench__DOT__dut__DOT__num_p2_wins 
                            = vlSelf->testbench__DOT__dut__DOT__num_p2_wins;
                    }
                    if ((1U & (IData)(vlSelf->testbench__DOT__dut__DOT__total_games))) {
                        __Vdly__testbench__DOT__dut__DOT__num_p1_wins 
                            = vlSelf->testbench__DOT__dut__DOT__num_p1_wins;
                        __Vdly__testbench__DOT__dut__DOT__num_p2_wins 
                            = (3U & ((IData)(vlSelf->testbench__DOT__dut__DOT__num_p2_wins) 
                                     - (IData)(3U)));
                    }
                }
                if (vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l) {
                    __Vdly__testbench__DOT__dut__DOT__total_games 
                        = (7U & ((IData)(7U) + (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)));
                    __Vdly__testbench__DOT__dut__DOT__curr_state_l = 1U;
                    __Vdly__testbench__DOT__dut__DOT__target_num 
                        = (7U & (IData)(vlSelf->testbench__DOT__dut__DOT__data_l));
                } else if (((IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l) 
                            | (IData)(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__idle_l))) {
                    __Vdly__testbench__DOT__dut__DOT__target_num 
                        = (7U & (IData)(vlSelf->testbench__DOT__dut__DOT__data_l));
                    __Vdly__testbench__DOT__dut__DOT__curr_state_l = 0U;
                } else {
                    __Vdly__testbench__DOT__dut__DOT__curr_state_l = 1U;
                }
            }
        } else if ((2U == (IData)(vlSelf->testbench__DOT__dut__DOT__curr_state_l))) {
            __Vdly__testbench__DOT__dut__DOT__curr_state_l = 0U;
            if (((IData)(vlSelf->testbench__DOT__dut__DOT__num_p1_wins) 
                 > (IData)(vlSelf->testbench__DOT__dut__DOT__num_p2_wins))) {
                vlSelf->testbench__DOT__dut__DOT__p1_w_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__p2_w_l = 0U;
                __Vdly__testbench__DOT__dut__DOT__num_p1_wins = 0U;
                __Vdly__testbench__DOT__dut__DOT__num_p2_wins = 0U;
            } else {
                vlSelf->testbench__DOT__dut__DOT__p1_w_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__p2_w_l = 1U;
                __Vdly__testbench__DOT__dut__DOT__num_p1_wins = 0U;
                __Vdly__testbench__DOT__dut__DOT__num_p2_wins = 0U;
            }
        } else {
            __Vdly__testbench__DOT__dut__DOT__curr_state_l = 0U;
            vlSelf->testbench__DOT__dut__DOT__p1_w_l = 0U;
            vlSelf->testbench__DOT__dut__DOT__p2_w_l = 0U;
            __Vdly__testbench__DOT__dut__DOT__num_p1_wins = 0U;
            __Vdly__testbench__DOT__dut__DOT__num_p2_wins = 0U;
            __Vdly__testbench__DOT__dut__DOT__total_games = 6U;
        }
        if ((4U & (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l))) {
            if ((2U & (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l))) {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 0U;
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l = 5U;
            } else if ((1U & (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l))) {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 0U;
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l = 5U;
            } else if ((0U == (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__games_l))) {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 0U;
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l = 5U;
            } else {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l 
                    = (7U & ((IData)(7U) + (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__games_l)));
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 1U;
            }
        } else if ((2U & (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l))) {
            if ((1U & (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l))) {
                if ((0U == (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__games_l))) {
                    __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 0U;
                    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l = 1U;
                    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 0U;
                    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
                    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 0U;
                    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 0U;
                    __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l = 5U;
                } else {
                    __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l 
                        = (7U & ((IData)(7U) + (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__games_l)));
                    __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 1U;
                    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 0U;
                    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 1U;
                }
            } else if (vlSelf->testbench__DOT__dut__DOT__d1_l) {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 3U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 1U;
            } else if (vlSelf->testbench__DOT__ten_sec_i) {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 4U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 1U;
            } else {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 2U;
            }
        } else if ((1U & (IData)(vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l))) {
            if (vlSelf->testbench__DOT__five_sec_i) {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 2U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 0U;
            } else {
                __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 1U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 0U;
                vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 0U;
            }
        } else if (vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__play_l) {
            __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 1U;
            vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l = 0U;
            vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 1U;
        } else {
            __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l = 0U;
            vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__nopl_l = 1U;
            vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__flas_l = 0U;
            vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__play_l = 0U;
            vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__wins_l = 0U;
            vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__lose_l = 0U;
        }
    }
    vlSelf->testbench__DOT__dut__DOT__curr_state_l 
        = __Vdly__testbench__DOT__dut__DOT__curr_state_l;
    vlSelf->testbench__DOT__dut__DOT__num_p1_wins = __Vdly__testbench__DOT__dut__DOT__num_p1_wins;
    vlSelf->testbench__DOT__dut__DOT__num_p2_wins = __Vdly__testbench__DOT__dut__DOT__num_p2_wins;
    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l 
        = __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__curr_state_l;
    vlSelf->testbench__DOT__dut__DOT__play_states_inst__DOT__games_l 
        = __Vdly__testbench__DOT__dut__DOT__play_states_inst__DOT__games_l;
    vlSelf->testbench__DOT__dut__DOT__d1_l = ((((~ (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)) 
                                                & (IData)(vlSelf->testbench__DOT__p1_press_i)) 
                                               | ((IData)(vlSelf->testbench__DOT__p2_press_i) 
                                                  & (IData)(vlSelf->testbench__DOT__dut__DOT__total_games))) 
                                              & (((6U 
                                                   & ((IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l) 
                                                      << 1U)) 
                                                  | (IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l)) 
                                                 == (IData)(vlSelf->testbench__DOT__dut__DOT__target_num)));
    __Vtableidx1 = (((IData)(vlSelf->testbench__DOT__start_i) 
                     << 6U) | (((IData)(vlSelf->testbench__DOT__dut__DOT__d2_l) 
                                << 5U) | (((0U == (IData)(vlSelf->testbench__DOT__dut__DOT__total_games)) 
                                           << 4U) | 
                                          (((IData)(vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__curr_state_l) 
                                            << 2U) 
                                           | (((IData)(vlSelf->testbench__DOT__reset_button) 
                                               << 1U) 
                                              | (IData)(vlSelf->testbench__DOT__reset_i))))));
    vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__curr_state_l 
        = Vtestbench__ConstPool__TABLE_h8558e017_0[__Vtableidx1];
    if ((2U & Vtestbench__ConstPool__TABLE_h28f2b6de_0
         [__Vtableidx1])) {
        vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__idle_l 
            = Vtestbench__ConstPool__TABLE_h4b211ce9_0
            [__Vtableidx1];
    }
    if ((4U & Vtestbench__ConstPool__TABLE_h28f2b6de_0
         [__Vtableidx1])) {
        vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__play_l 
            = Vtestbench__ConstPool__TABLE_hf60ba374_0
            [__Vtableidx1];
    }
    if ((8U & Vtestbench__ConstPool__TABLE_h28f2b6de_0
         [__Vtableidx1])) {
        vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__win_l 
            = Vtestbench__ConstPool__TABLE_ha498e8eb_0
            [__Vtableidx1];
    }
    if ((0x10U & Vtestbench__ConstPool__TABLE_h28f2b6de_0
         [__Vtableidx1])) {
        vlSelf->testbench__DOT__dut__DOT__game_states_inst__DOT__no_win_l 
            = Vtestbench__ConstPool__TABLE_h71834f84_0
            [__Vtableidx1];
    }
    vlSelf->testbench__DOT__dut__DOT__target_num = __Vdly__testbench__DOT__dut__DOT__target_num;
    vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l 
        = __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l;
    vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l 
        = __Vdly__testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l;
    vlSelf->testbench__DOT__dut__DOT__d2_l = __Vdly__testbench__DOT__dut__DOT__d2_l;
    vlSelf->testbench__DOT__dut__DOT__total_games = __Vdly__testbench__DOT__dut__DOT__total_games;
    vlSelf->testbench__DOT__dut__DOT__data_l = (((IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__data_l) 
                                                 << 1U) 
                                                | (IData)(vlSelf->testbench__DOT__dut__DOT__lfsr_inst__DOT__first_bit_l));
}

void Vtestbench___024root___eval_nba(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_nba\n"); );
    // Body
    if (vlSelf->__VnbaTriggered.at(1U)) {
        Vtestbench___024root___nba_sequent__TOP__0(vlSelf);
    }
    if (vlSelf->__VnbaTriggered.at(2U)) {
        Vtestbench___024root___nba_sequent__TOP__1(vlSelf);
    }
    if (vlSelf->__VnbaTriggered.at(0U)) {
        Vtestbench___024root___nba_sequent__TOP__2(vlSelf);
        vlSelf->__Vm_traceActivity[3U] = 1U;
    }
    if (vlSelf->__VnbaTriggered.at(3U)) {
        Vtestbench___024root___nba_sequent__TOP__3(vlSelf);
        vlSelf->__Vm_traceActivity[4U] = 1U;
    }
}

void Vtestbench___024root___eval_triggers__act(Vtestbench___024root* vlSelf);
void Vtestbench___024root___timing_commit(Vtestbench___024root* vlSelf);
#ifdef VL_DEBUG
VL_ATTR_COLD void Vtestbench___024root___dump_triggers__act(Vtestbench___024root* vlSelf);
#endif  // VL_DEBUG
void Vtestbench___024root___timing_resume(Vtestbench___024root* vlSelf);
#ifdef VL_DEBUG
VL_ATTR_COLD void Vtestbench___024root___dump_triggers__nba(Vtestbench___024root* vlSelf);
#endif  // VL_DEBUG

void Vtestbench___024root___eval(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval\n"); );
    // Init
    VlTriggerVec<5> __VpreTriggered;
    IData/*31:0*/ __VnbaIterCount;
    CData/*0:0*/ __VnbaContinue;
    // Body
    __VnbaIterCount = 0U;
    __VnbaContinue = 1U;
    while (__VnbaContinue) {
        __VnbaContinue = 0U;
        vlSelf->__VnbaTriggered.clear();
        vlSelf->__VactIterCount = 0U;
        vlSelf->__VactContinue = 1U;
        while (vlSelf->__VactContinue) {
            vlSelf->__VactContinue = 0U;
            Vtestbench___024root___eval_triggers__act(vlSelf);
            Vtestbench___024root___timing_commit(vlSelf);
            if (vlSelf->__VactTriggered.any()) {
                vlSelf->__VactContinue = 1U;
                if (VL_UNLIKELY((0x64U < vlSelf->__VactIterCount))) {
#ifdef VL_DEBUG
                    Vtestbench___024root___dump_triggers__act(vlSelf);
#endif
                    VL_FATAL_MT("testbench.sv", 4, "", "Active region did not converge.");
                }
                vlSelf->__VactIterCount = ((IData)(1U) 
                                           + vlSelf->__VactIterCount);
                __VpreTriggered.andNot(vlSelf->__VactTriggered, vlSelf->__VnbaTriggered);
                vlSelf->__VnbaTriggered.set(vlSelf->__VactTriggered);
                Vtestbench___024root___timing_resume(vlSelf);
                Vtestbench___024root___eval_act(vlSelf);
            }
        }
        if (vlSelf->__VnbaTriggered.any()) {
            __VnbaContinue = 1U;
            if (VL_UNLIKELY((0x64U < __VnbaIterCount))) {
#ifdef VL_DEBUG
                Vtestbench___024root___dump_triggers__nba(vlSelf);
#endif
                VL_FATAL_MT("testbench.sv", 4, "", "NBA region did not converge.");
            }
            __VnbaIterCount = ((IData)(1U) + __VnbaIterCount);
            Vtestbench___024root___eval_nba(vlSelf);
        }
    }
}

void Vtestbench___024root___timing_commit(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___timing_commit\n"); );
    // Body
    if ((1U & (~ (IData)(vlSelf->__VactTriggered.at(1U))))) {
        vlSelf->__VtrigSched_h382f1e33__0.commit("@(negedge testbench.reset_i)");
    }
    if ((1U & (~ (IData)(vlSelf->__VactTriggered.at(3U))))) {
        vlSelf->__VtrigSched_hef748430__0.commit("@(posedge testbench.clk_i)");
    }
}

void Vtestbench___024root___timing_resume(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___timing_resume\n"); );
    // Body
    if (vlSelf->__VactTriggered.at(1U)) {
        vlSelf->__VtrigSched_h382f1e33__0.resume("@(negedge testbench.reset_i)");
    }
    if (vlSelf->__VactTriggered.at(3U)) {
        vlSelf->__VtrigSched_hef748430__0.resume("@(posedge testbench.clk_i)");
    }
    if (vlSelf->__VactTriggered.at(4U)) {
        vlSelf->__VdlySched.resume();
    }
}

#ifdef VL_DEBUG
void Vtestbench___024root___eval_debug_assertions(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_debug_assertions\n"); );
}
#endif  // VL_DEBUG
