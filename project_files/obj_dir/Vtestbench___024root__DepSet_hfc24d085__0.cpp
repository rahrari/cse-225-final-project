// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtestbench.h for the primary calling header

#include "verilated.h"

#include "Vtestbench__Syms.h"
#include "Vtestbench___024root.h"

VL_INLINE_OPT VlCoroutine Vtestbench___024root___eval_initial__TOP__0(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial__TOP__0\n"); );
    // Init
    VlWide<4>/*127:0*/ __Vtemp_h9849538a__0;
    // Body
    __Vtemp_h9849538a__0[0U] = 0x2e667374U;
    __Vtemp_h9849538a__0[1U] = 0x61746f72U;
    __Vtemp_h9849538a__0[2U] = 0x6572696cU;
    __Vtemp_h9849538a__0[3U] = 0x76U;
    vlSymsp->_vm_contextp__->dumpfile(VL_CVT_PACK_STR_NW(4, __Vtemp_h9849538a__0));
    vlSymsp->_traceDumpOpen();
    VL_WRITEF("\n  ______          __  __                    __      \n /_  __/__  _____/ /_/ /_  ___  ____  _____/ /_     \n  / / / _ \\/ ___/ __/ __ \\/ _ \\/ __ \\/ ___/ __ \\\n / / /  __(__  ) /_/ /_/ /  __/ / / / /__/ / / /  / /\n/_/  \\___/____/\\__/_.___/\\___/_/ /_/\\___/_/ /_/  /_/\n\nBegin Test:\n");
    vlSelf->testbench__DOT__itervar = 0U;
    vlSelf->testbench__DOT__ten_sec_i = (1U & (vlSelf->testbench__DOT__test_symbols
                                               [0U] 
                                               >> 8U));
    vlSelf->testbench__DOT__five_sec_i = (1U & (vlSelf->testbench__DOT__test_symbols
                                                [0U] 
                                                >> 7U));
    vlSelf->testbench__DOT__p2_press_i = (1U & (vlSelf->testbench__DOT__test_symbols
                                                [0U] 
                                                >> 6U));
    vlSelf->testbench__DOT__p1_press_i = (1U & (vlSelf->testbench__DOT__test_symbols
                                                [0U] 
                                                >> 5U));
    vlSelf->testbench__DOT__start_i = (1U & (vlSelf->testbench__DOT__test_symbols
                                             [0U] >> 4U));
    vlSelf->testbench__DOT__correct_p2_wins_o = (1U 
                                                 & (vlSelf->testbench__DOT__test_symbols
                                                    [0U] 
                                                    >> 3U));
    vlSelf->testbench__DOT__correct_p1_wins_o = (1U 
                                                 & (vlSelf->testbench__DOT__test_symbols
                                                    [0U] 
                                                    >> 2U));
    vlSelf->testbench__DOT__reset_button = (1U & (vlSelf->testbench__DOT__test_symbols
                                                  [0U] 
                                                  >> 1U));
    vlSelf->testbench__DOT__no_input = (1U & vlSelf->testbench__DOT__test_symbols
                                        [0U]);
    co_await vlSelf->__VtrigSched_h382f1e33__0.trigger(
                                                       "@(negedge testbench.reset_i)", 
                                                       "testbench.sv", 
                                                       160);
    vlSelf->__Vm_traceActivity[2U] = 1U;
    vlSelf->testbench__DOT__reset_done = 1U;
    vlSelf->testbench__DOT__correct_p2_wins_o = 0U;
    vlSelf->testbench__DOT__correct_p1_wins_o = 0U;
    vlSelf->testbench__DOT__itervar = 0U;
    while (VL_GTES_III(32, 0x49U, vlSelf->testbench__DOT__itervar)) {
        vlSelf->testbench__DOT__correct_p2_wins_o = 
            (1U & (((0x49U >= (0x7fU & vlSelf->testbench__DOT__itervar))
                     ? vlSelf->testbench__DOT__test_symbols
                    [(0x7fU & vlSelf->testbench__DOT__itervar)]
                     : 0U) >> 3U));
        vlSelf->testbench__DOT__correct_p1_wins_o = 
            (1U & (((0x49U >= (0x7fU & vlSelf->testbench__DOT__itervar))
                     ? vlSelf->testbench__DOT__test_symbols
                    [(0x7fU & vlSelf->testbench__DOT__itervar)]
                     : 0U) >> 2U));
        co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                           "@(posedge testbench.clk_i)", 
                                                           "testbench.sv", 
                                                           169);
        vlSelf->__Vm_traceActivity[2U] = 1U;
        VL_WRITEF("At Posedge %11d: Ten_sec: %b, Five_sec: %b, P1_press: %b, P2_press: %b,  Start_i: %b, R:%b reset_i = %b \n",
                  32,vlSelf->testbench__DOT__itervar,
                  1,(IData)(vlSelf->testbench__DOT__ten_sec_i),
                  1,vlSelf->testbench__DOT__five_sec_i,
                  1,(IData)(vlSelf->testbench__DOT__p1_press_i),
                  1,vlSelf->testbench__DOT__p2_press_i,
                  1,(IData)(vlSelf->testbench__DOT__start_i),
                  1,vlSelf->testbench__DOT__reset_button,
                  1,(IData)(vlSelf->testbench__DOT__reset_i));
        vlSelf->testbench__DOT__itervar = ((IData)(1U) 
                                           + vlSelf->testbench__DOT__itervar);
    }
    vlSelf->__Vm_traceActivity[2U] = 1U;
}

#ifdef VL_DEBUG
VL_ATTR_COLD void Vtestbench___024root___dump_triggers__act(Vtestbench___024root* vlSelf);
#endif  // VL_DEBUG

void Vtestbench___024root___eval_triggers__act(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_triggers__act\n"); );
    // Body
    vlSelf->__VactTriggered.at(0U) = ((~ (IData)(vlSelf->testbench__DOT__clk_i)) 
                                      & (IData)(vlSelf->__Vtrigrprev__TOP__testbench__DOT__clk_i));
    vlSelf->__VactTriggered.at(1U) = ((~ (IData)(vlSelf->testbench__DOT__reset_i)) 
                                      & (IData)(vlSelf->__Vtrigrprev__TOP__testbench__DOT__reset_i));
    vlSelf->__VactTriggered.at(2U) = ((IData)(vlSelf->testbench__DOT__reset_i) 
                                      & (~ (IData)(vlSelf->__Vtrigrprev__TOP__testbench__DOT__reset_i)));
    vlSelf->__VactTriggered.at(3U) = ((IData)(vlSelf->testbench__DOT__clk_i) 
                                      & (~ (IData)(vlSelf->__Vtrigrprev__TOP__testbench__DOT__clk_i)));
    vlSelf->__VactTriggered.at(4U) = vlSelf->__VdlySched.awaitingCurrentTime();
    vlSelf->__Vtrigrprev__TOP__testbench__DOT__clk_i 
        = vlSelf->testbench__DOT__clk_i;
    vlSelf->__Vtrigrprev__TOP__testbench__DOT__reset_i 
        = vlSelf->testbench__DOT__reset_i;
#ifdef VL_DEBUG
    if (VL_UNLIKELY(vlSymsp->_vm_contextp__->debug())) {
        Vtestbench___024root___dump_triggers__act(vlSelf);
    }
#endif
}
