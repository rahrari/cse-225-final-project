module clock
   (input [0:0] clk_i
   ,input [0:0] reset_i
   ,input [0:0] reset_button_i
   ,output [0:0] eigth_sec_o
   ,output [0:0] five_sec_o
   ,output [0:0] ten_sec_o);

   // Implement a clock module that outputs when certain time
   // thresholds have been met
   // Your code here:
   
   logic [31:0] counter_l;
   
   counter
   #(.width_p(32))
   count_inst
   (.clk_i(clk_i)
   ,.reset_i(reset_i | reset_button_i)
   ,.up_i(1'b1)
   ,.down_i(1'b0)
   ,.counter_o(counter_l));
   
   assign eigth_sec_o = counter_l[24]; //Roughly 1/8 Sec
   assign five_sec_o = counter_l[27] & counter_l[29]; //Roughly 5 Secs
   assign ten_sec_o = counter_l[28] & counter_l[30]; //Roughly 10 Secs
	
endmodule
