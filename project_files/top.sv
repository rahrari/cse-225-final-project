module top
   (input [0:0] clk_i
   ,input [0:0] reset_i
   ,input [0:0] quit_button_i   // Input for purposes of simulation
   ,input [0:0] start_i         // Input for purposes of simulation
   ,input [0:0] p1_press_i      // Input for purposes of simulation
   ,input [0:0] p2_press_i      // Input for purposes of simulation
   ,input [0:0] five_sec_i      // Input for purposes of simulation
   ,input [0:0] ten_sec_i       // Input for purposes of simulation
   //,input [3:0] kypd_1_i      // Input for purposes of hardware implementation (if I got to it)
   //,input [3:0] kypd_2_i      // Input for purposes of hardware implementation (if I got to it)
   ,output [0:0] p1_wins_o      // Output for purposes of simulation
   ,output [0:0] p2_wins_o      // Output for purposes of simulation
   //,output [6:0] sev_seg1     // Output for purposes of hardware implementation (if I got to it)
   //,output [6:0] sev_seg2     // Output for purposes of hardware implementation (if I got to it)
   );
   
   // Vars from lines 19 to 35 are for this specific module
   logic [0:0] p1_w_l;
   logic [0:0] p2_w_l;
   
   logic [2:0] total_games;
   
   enum logic [1:0] {IDLE = 2'b00,
                     PLAY = 2'b01,
                     WIN = 2'b10
                     }
                     state_r;
      
   logic [1:0] curr_state_l;
   
   logic [1:0] num_p1_wins; //Track number of wins for p1
   logic [1:0] num_p2_wins; //Track number of wins for p2
   
   logic [2:0] target_num; //Used to store goal/target for each round
   
   // Vars from lines 38 to 41 are for game_states module
   logic [0:0] gs_idle_o_l;
   logic [0:0] gs_play_o_l;
   logic [0:0] gs_win_o_l;
   logic [0:0] gs_no_win_o_l;
   
   // Vars from lines 44 to 48 are for play_states module
   logic [0:0] ps_nopl_o_l;
   logic [0:0] ps_flas_o_l;
   logic [0:0] ps_play_o_l;
   logic [0:0] ps_win_o_l;
   logic [0:0] ps_lose_o_l;
   
   // Lines 51 to 64 would be used in the hardware case 
   //logic [0:0] eigth_sec_l;
   //logic [0:0] five_sec_l;
   //logic [0:0] ten_sec_l;
   
   //clock
   //#()
   //clock_inst
   //(.clk_i(clk_i)
   //,.reset_i(reset_i)
   //,.reset_button_i()
   //,.eigth_sec_o(eigth_sec_l)
   //,.five_sec_o(five_sec_l)
   //,.ten_sec_o(ten_sec_l)
   //);
   
   //Var below used for lfsr
   logic [4:0] data_l;
   
   //Var below used to determine if p1 or p2 has won in a round
   logic [0:0] win_l;
   logic [0:0] d1_l;
   logic [0:0] w_l;
   logic [0:0] d2_l;
   
   assign win_l = ( ( ( (p1_press_i && total_games[0] == 1'b0) || (p2_press_i && total_games[0] == 1'b1) ) && (data_l[2:0] == target_num)));
   assign w_l = (((p1_w_l > p2_w_l) || (p2_w_l > p1_w_l)) && (total_games == '0) );
   
   lfsr
   #()
   lfsr_inst
   (.clk_i(clk_i)
   //.clk_i(eigth_sec_l)    //With HW, we use the 1/8 sec signal to flash through the LFSR
   ,.reset_i(reset_i)
   ,.data_o(data_l));
   
   play_states
   #()
   play_states_inst
   (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.in_play_i(gs_play_o_l)
   ,.five_sec_i(five_sec_i)
   ,.ten_sec_i(ten_sec_i)
   ,.win_i(d1_l)
   ,.quit_i(quit_button_i)
   ,.nopl_o(ps_nopl_o_l)
   ,.flas_o(ps_flas_o_l)
   ,.play_o(ps_play_o_l)
   ,.wins_o(ps_win_o_l)
   ,.lose_o(ps_lose_o_l));
   
   game_states
   #()
   game_states_inst
   (.clk_i(clk_i)
   ,.reset_i(reset_i)
   ,.start_i(start_i)
   ,.three_played_i((total_games == '0))
   ,.w_i(d2_l)
   ,.quit_i(quit_button_i)
   ,.idle_o(gs_idle_o_l)
   ,.play_o(gs_play_o_l)
   ,.win_o(gs_win_o_l)
   ,.no_win_o(gs_no_win_o_l));
   
   always_ff @(posedge clk_i) begin
   	d1_l <= win_l;
   	d2_l <= w_l; 
   	if (reset_i || quit_button_i) begin
   		curr_state_l <= IDLE;
   		p1_w_l <= 1'b0;
   		p2_w_l <= 1'b0;
   		num_p1_wins <= 2'b00;
   		num_p2_wins <= 2'b00;
   		total_games <= 3'b110;
   	end else begin
   		case(curr_state_l)
   			IDLE:
   				begin
   					if(gs_play_o_l) begin
   						curr_state_l <= PLAY;
   						p1_w_l <= 1'b0;
			   			p2_w_l <= 1'b0;
			   			num_p1_wins <= 2'b00;
			   			num_p2_wins <= 2'b00;
			   			target_num <= data_l[2:0];
			   			total_games <= 3'b110;
   					end else begin
   						curr_state_l <= IDLE;
   						p1_w_l <= 1'b0;
			   			p2_w_l <= 1'b0;
			   			num_p1_wins <= 2'b00;
			   			num_p2_wins <= 2'b00;
			   			total_games <= 3'b110;
   					end
   				end
   			PLAY:
   				begin
   					if(total_games == '0) begin
   						if( ((num_p1_wins > num_p2_wins) || (num_p1_wins > num_p2_wins)) ) begin
   							curr_state_l <= WIN;
   						end else begin
   							curr_state_l <= IDLE;
   						end
   					end else begin
   						if( ps_win_o_l ) begin
   							curr_state_l <= PLAY;
   							total_games <= total_games + '1;
   							target_num <= data_l[2:0];
   							if(total_games[0] == 1'b0) begin
   								num_p1_wins <= num_p1_wins -'1;
			   					num_p2_wins <= num_p2_wins;
   							end 
   							if (total_games[0] == 1'b1) begin
   								num_p1_wins <= num_p1_wins;
			   					num_p2_wins <= num_p2_wins - '1;
   							end
   						end
   						if( ps_lose_o_l ) begin
   							curr_state_l <= PLAY;
   							total_games <= total_games + '1;
   							target_num <= data_l[2:0];
   						end else begin
   							if((ps_nopl_o_l || gs_idle_o_l)) begin
   								target_num <= data_l[2:0];
   								curr_state_l <= IDLE;
   							end else begin
   								curr_state_l <= PLAY;
   							end
   						end
   					end
   				end
   			WIN:
   				begin
   					curr_state_l <= IDLE;
   					if(num_p1_wins > num_p2_wins) begin
   						p1_w_l <= 1'b1;
				   		p2_w_l <= 1'b0;
				   		num_p1_wins <= 2'b00;
				   		num_p2_wins <= 2'b00;
   					end else begin
   						p1_w_l <= 1'b0;
				   		p2_w_l <= 1'b1;
				   		num_p1_wins <= 2'b00;
				   		num_p2_wins <= 2'b00;
   					end
   				end
   			default:
   				begin
   					curr_state_l <= IDLE;
   					p1_w_l <= 1'b0;
			   		p2_w_l <= 1'b0;
			   		num_p1_wins <= 2'b00;
			   		num_p2_wins <= 2'b00;
			   		total_games <= 3'b110;
   				end
   		endcase
   	end
   end
   
   assign p1_wins_o = p1_w_l;
   assign p2_wins_o = p2_w_l;
   
endmodule
