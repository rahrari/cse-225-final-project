module lfsr
   (input [0:0] clk_i
   ,input [0:0] reset_i
   ,output [4:0] data_o);

   // Implement a 5-bit Many-to-One Linear Feedback Shift Register
   //
   // *** You may use any previously created module from this lab ***
   //
   // Your solution must include behavioral verilog written in this file.
   // 
   // Your LFSR should reset to 5'b00001 at the positive edge of clk_i when reset_i is high
   // 
   // Your code here:
   	logic [3:0] data_l;
	logic [0:0] first_bit_l;
	always_ff @(posedge clk_i) begin
		if (reset_i) begin
			data_l <= 4'b0000;
			first_bit_l <= 1'b1;
		end else begin
			first_bit_l <= data_l[0] ^ data_l[3];
			data_l <= {data_l[2:0], first_bit_l};		
		end
	end 
	
	assign data_o = {data_l, first_bit_l};

endmodule
