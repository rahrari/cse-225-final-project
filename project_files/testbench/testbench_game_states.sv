// Ryan Ahrari Final Proj Testbench file

`timescale 1ns/1ps
module testbench();
   localparam iterations_lp = 32;
   logic [0:0] reset_done = 1'b0;

   wire [0:0]  clk_i;
   wire [0:0]  reset_i;
   logic [0:0] reset_button; //quit_i input for game states
   logic [0:0] start_i;
   logic [0:0] three_played_i;
   logic [0:0] win_i;
   logic [0:0] no_input;
   wire [0:0]  idle_o;
   wire [0:0]  play_o;
   wire [0:0]  win_o;
   wire [0:0]  no_win_o;

   wire [0:0]  error_idle_o;
   logic [0:0] correct_idle_o;
   logic [0:0] correct_idle_n;
   
   wire [0:0]  error_play_o;
   logic [0:0] correct_play_o;
   logic [0:0] correct_play_n;
   
   wire [0:0]  error_win_o;
   logic [0:0] correct_win_o;
   logic [0:0] correct_win_n;
   
   wire [0:0]  error_no_win_o;
   logic [0:0] correct_no_win_o;
   logic [0:0] correct_no_win_n;

   int         itervar;
   logic [8:0] test_symbols [33];

   initial begin
      // {Win, 3 Games Played, Start, NOWIN, WIN, PLAY, IDLE, Reset, No Input}
      test_symbols[ 0] = 9'b0_0000_0001;
      test_symbols[ 1] = 9'b0_0100_0100;
      test_symbols[ 2] = 9'b0_1000_1000;
      test_symbols[ 3] = 9'b1_1110_0000;
      test_symbols[ 4] = 9'b1_1000_0100;
      test_symbols[ 5] = 9'b0_1000_0100;
      test_symbols[ 6] = 9'b1_0000_0100;
      test_symbols[ 7] = 9'b0_1000_0100;
      test_symbols[ 8] = 9'b1_1000_0100;
      test_symbols[ 9] = 9'b0_0100_0100;
      test_symbols[10] = 9'b1_1000_1000;
      test_symbols[11] = 9'b1_1101_0000;
      test_symbols[12] = 9'b0_0000_0100;
      test_symbols[13] = 9'b0_0000_0100;
      test_symbols[14] = 9'b0_0000_0110;
      test_symbols[15] = 9'b0_0100_0100;
      test_symbols[16] = 9'b1_0100_1000;
      test_symbols[17] = 9'b1_0000_1000;
      test_symbols[18] = 9'b0_0100_1000;
      test_symbols[19] = 9'b1_1100_1000;
      test_symbols[20] = 9'b0_1001_0000;
      test_symbols[21] = 9'b1_1000_0100;
      test_symbols[22] = 9'b0_0100_0100;
      test_symbols[23] = 9'b1_1000_1000;
      test_symbols[24] = 9'b1_1101_0000;
      test_symbols[25] = 9'b0_0000_0100;
      test_symbols[26] = 9'b0_0000_0100;
      test_symbols[27] = 9'b0_0100_0100;
      test_symbols[28] = 9'b0_1000_1000;
      test_symbols[29] = 9'b1_1110_0000;
      test_symbols[30] = 9'b1_1000_0100;
      test_symbols[31] = 9'b0_1000_0100;
      test_symbols[32] = 9'b1_0000_0100;
   end
   
   nonsynth_clock_gen
     #(.cycle_time_p(10))
   cg
     (.clk_o(clk_i));

   nonsynth_reset_gen
     #(.num_clocks_p(1)
       ,.reset_cycles_lo_p(1)
       ,.reset_cycles_hi_p(10))
   rg
     (.clk_i(clk_i)
      ,.async_reset_o(reset_i));

  game_states
     #()
   dut
     (.clk_i(clk_i)
     ,.reset_i(reset_i | reset_button)
     ,.start_i(start_i)
     ,.three_played_i(three_played_i)
     ,.w_i(win_i)
     ,.quit_i(reset_button)
     ,.idle_o(idle_o)
     ,.play_o(play_o)
     ,.win_o(win_o)
     ,.no_win_o(no_win_o));

   initial begin
      reset_button = 0;      
`ifdef VERILATOR
      $dumpfile("verilator.fst");
`else
      $dumpfile("iverilog.vcd");
`endif
      $dumpvars;

      $display();
      $display("  ______          __  __                    __      ");
      $display(" /_  __/__  _____/ /_/ /_  ___  ____  _____/ /_     ");
      $display("  / / / _ \\/ ___/ __/ __ \\/ _ \\/ __ \\/ ___/ __ \\");
      $display(" / / /  __(__  ) /_/ /_/ /  __/ / / / /__/ / / /  / /");
      $display("/_/  \\___/____/\\__/_.___/\\___/_/ /_/\\___/_/ /_/  /_/");
                                                                                        
      $display();
      $display("Begin Test:");

      itervar = 0;
      {win_i, three_played_i, start_i, correct_no_win_o, correct_win_o, correct_play_o, correct_idle_o, reset_button, no_input} = test_symbols[itervar];

      @(negedge reset_i);

      reset_done = 1'b1;
      correct_no_win_o = 1'b0;
      correct_win_o = 1'b0;
      correct_play_o = 1'b0;
      correct_idle_o = 1'b0;

      for(itervar = 0; itervar <= iterations_lp; itervar ++) begin
         correct_no_win_o = test_symbols[itervar][5];
         correct_win_o = test_symbols[itervar][4];
         correct_play_o = test_symbols[itervar][3];
         correct_idle_o = test_symbols[itervar][2];
	 @(posedge clk_i);
	 $display("At Posedge %d: W: %b 3_Games_Played: %b, start: %b, R:%b reset_i = %b ", itervar, win_i, three_played_i, start_i, reset_button, reset_i);
      end
      
   end

   assign error_idle_o = (correct_idle_o !== idle_o) && (itervar < iterations_lp);
   assign error_play_o = (correct_play_o !== play_o) && (itervar < iterations_lp);
   assign error_win_o = (correct_win_o !== win_o) && (itervar < iterations_lp);
   assign error_no_win_o = (correct_no_win_o !== no_win_o) && (itervar < iterations_lp);
   always @(negedge clk_i) begin
      // {Up,Down,Left,Right,B,A,Start,Reset,Unlock,No Input}
      {win_i, three_played_i, start_i, correct_no_win_n, correct_win_n, correct_play_n, correct_idle_n, reset_button, no_input} = test_symbols[itervar];
      if(!reset_i && (reset_done == 1) && (error_idle_o == 1)) begin
         $display("\033[0;31mError!\033[0m: idle_o should be %b, but got %b", correct_idle_o, idle_o);
         $finish();
      end
      if(!reset_i && (reset_done == 1) && (error_play_o == 1)) begin
         $display("\033[0;31mError!\033[0m: play_o should be %b, but got %b", correct_play_o, play_o);
         $finish();
      end
      if(!reset_i && (reset_done == 1) && (error_win_o == 1)) begin
         $display("\033[0;31mError!\033[0m: win_o should be %b, but got %b", correct_win_o, win_o);
         $finish();
      end
      if(!reset_i && (reset_done == 1) && (error_no_win_o == 1)) begin
         $display("\033[0;31mError!\033[0m: no_win_o should be %b, but got %b", correct_no_win_o, no_win_o);
         $finish();
      end
      if (itervar >= iterations_lp)
        $finish();
   end

   final begin
      $display("Simulation time is %t", $time);
      if(error_idle_o || error_play_o || error_win_o || error_no_win_o) begin
	 $display("\033[0;31m    ______                    \033[0m");
	 $display("\033[0;31m   / ____/_____________  _____\033[0m");
	 $display("\033[0;31m  / __/ / ___/ ___/ __ \\/ ___/\033[0m");
	 $display("\033[0;31m / /___/ /  / /  / /_/ / /    \033[0m");
	 $display("\033[0;31m/_____/_/  /_/   \\____/_/     \033[0m");
	 $display();
	 $display("Simulation Failed");
      end else begin
	 $display("\033[0;32m    ____  ___   __________\033[0m");
	 $display("\033[0;32m   / __ \\/   | / ___/ ___/\033[0m");
	 $display("\033[0;32m  / /_/ / /| | \\__ \\\__ \ \033[0m");
	 $display("\033[0;32m / ____/ ___ |___/ /__/ / \033[0m");
	 $display("\033[0;32m/_/   /_/  |_/____/____/  \033[0m");
	 $display();
	 $display("Simulation Succeeded!");
      end // else: !if(error_unlock_o)
   end

endmodule
