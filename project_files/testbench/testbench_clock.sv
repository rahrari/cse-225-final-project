// Ryan Ahrari Final Proj Testbench file

// I would NOT run this file, the simulation takes way too long to complete

`timescale 1ns/1ps
module testbench();
   localparam iterations_lp = (2**32);
   
   logic [0:0] reset_done = 1'b0;
   logic [0:0] reset_button_i;
   wire [0:0]  clk_i;
   wire [0:0]  reset_i;
   
   wire [0:0]  eigth_sec_o;
   wire [0:0]  five_sec_o;
   wire [0:0]  ten_sec_o;
   
   wire [31:0]  counter_o;
   
   int			  itervar;

   //assign error_counter_o = (counter_o !== correct_counter_o);

   nonsynth_clock_gen
     #(.cycle_time_p(10))
   cg
     (.clk_o(clk_i));

   nonsynth_reset_gen
     #(.num_clocks_p(1)
      ,.reset_cycles_lo_p(1)
      ,.reset_cycles_hi_p(10))
   rg
     (.clk_i(clk_i)
     ,.async_reset_o(reset_i));

   clock
     #()
   dut
     (.clk_i(clk_i)
     ,.reset_i(reset_i)
     ,.reset_button_i(reset_button_i)
     ,.eight_sec_o(eigth_sec_o)
     ,.five_sec_o(five_sec_o)
     ,.ten_sec_o(ten_sec_o));
     
   counter
     #(.width_p(32))
   counter_inst
     (.clk_i(clk_i)
     ,.reset_i(reset_i)
     ,.up_i(1'b1)
     ,.down_i(1'b0)
     ,.counter_o(counter_o));

   initial begin
`ifdef VERILATOR
      $dumpfile("verilator.fst");
`else
      $dumpfile("iverilog.vcd");
`endif
      $dumpvars;

      $display();
      $display("  ______          __  __                    __            __    _ ______ ");
      $display(" /_  __/__  _____/ /_/ /_  ___  ____  _____/ /_     _____/ /_  (_) __/ /_");
      $display("  / / / _ \\/ ___/ __/ __ \\/ _ \\/ __ \\/ ___/ __ \\   / ___/ __ \\/ / /_/ __/");
      $display(" / / /  __(__  ) /_/ /_/ /  __/ / / / /__/ / / /  (__  ) / / / / __/ /_  ");
      $display("/_/  \\___/____/\\__/_.___/\\___/_/ /_/\\___/_/ /_/  /____/_/ /_/_/_/  \\__/  ");

      $display();
      $display("Begin Test:");

      itervar = 0;
      //correct_counter_o = '0;

      @(negedge reset_i);

      reset_done = 1;

      for(itervar = 0; itervar < iterations_lp ; itervar ++) begin
	 @(posedge clk_i);
	 if (counter_o[24] || counter_o[27]|| counter_o[28]|| counter_o[29]|| counter_o[30]) begin
		 $display("At Posedge %d: eigth_sec_o = %b, five_sec_o = %b, ten_sec_o = %b, reset_i = %b ",
			  itervar, eigth_sec_o, down_i, correct_counter_o, reset_i);
          end
      end
      $finish();
   end


   final begin
      $display("Simulation time is %t", $time);
      $display("\033[0;32m    ____  ___   __________\033[0m");
      $display("\033[0;32m   / __ \\/   | / ___/ ___/\033[0m");
      $display("\033[0;32m  / /_/ / /| | \\__ \\\__ \ \033[0m");
      $display("\033[0;32m / ____/ ___ |___/ /__/ / \033[0m");
      $display("\033[0;32m/_/   /_/  |_/____/____/  \033[0m");
      $display();
      $display("Simulation Succeeded!");
      end
   end

endmodule
