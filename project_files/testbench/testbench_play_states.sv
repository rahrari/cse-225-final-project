// Ryan Ahrari Final Proj Testbench file

`timescale 1ns/1ps
module testbench();
   localparam iterations_lp = 36;
   logic [0:0] reset_done = 1'b0;

   wire [0:0]  clk_i;
   wire [0:0]  reset_i;
   logic [0:0] reset_button; //quit_i input for play states
   logic [0:0] in_play_i;
   logic [0:0] five_sec_i;
   logic [0:0] ten_sec_i;
   logic [0:0] win_i;
   logic [0:0] no_input;
   wire [0:0]  nopl_o;
   wire [0:0]  flas_o;
   wire [0:0]  play_o;
   wire [0:0]  win_o;
   wire [0:0]  lose_o;

   wire [0:0]  error_nopl_o;
   logic [0:0] correct_nopl_o;
   logic [0:0] correct_nopl_n;
   
   wire [0:0]  error_flas_o;
   logic [0:0] correct_flas_o;
   logic [0:0] correct_flas_n;
   
   wire [0:0]  error_play_o;
   logic [0:0] correct_play_o;
   logic [0:0] correct_play_n;
   
   wire [0:0]  error_win_o;
   logic [0:0] correct_win_o;
   logic [0:0] correct_win_n;
   
   wire [0:0]  error_lose_o;
   logic [0:0] correct_lose_o;
   logic [0:0] correct_lose_n;

   int         itervar;
   logic [10:0] test_symbols [37];

   initial begin
      // {Win, Ten-Sec, Five-Sec, In-Play, LOSE, WINS, PLAY, FLAS, NOPL, Reset, No Input}
      test_symbols[ 0] = 11'b000_0000_0001;
      test_symbols[ 1] = 11'b000_1000_0100;
      test_symbols[ 2] = 11'b000_0000_1000;
      test_symbols[ 3] = 11'b000_0000_1000;
      test_symbols[ 4] = 11'b001_0000_1000;
      test_symbols[ 5] = 11'b000_1001_0000;
      test_symbols[ 6] = 11'b000_0001_0000;
      test_symbols[ 7] = 11'b001_0001_0000;
      test_symbols[ 8] = 11'b001_1001_0000;
      test_symbols[ 9] = 11'b010_0001_0000;
      test_symbols[10] = 11'b000_0100_0000; //Game 1 done P1 Lose
      test_symbols[11] = 11'b111_0000_1000;
      test_symbols[12] = 11'b000_0000_1000;
      test_symbols[13] = 11'b010_0000_1000;
      test_symbols[14] = 11'b100_0000_1000;
      test_symbols[15] = 11'b110_0000_1000;
      test_symbols[16] = 11'b001_0000_1000;
      test_symbols[17] = 11'b101_0001_0000;
      test_symbols[18] = 11'b100_0001_0000;
      test_symbols[19] = 11'b001_0001_0000;
      test_symbols[20] = 11'b110_0001_0000;
      test_symbols[21] = 11'b111_0010_0000; //Game 2 done P2 Win
      test_symbols[22] = 11'b000_0000_1000;
      test_symbols[23] = 11'b001_0000_1000;
      test_symbols[24] = 11'b110_0001_0000;
      test_symbols[25] = 11'b000_0010_0000; //Game 3 done P1 Win
      test_symbols[26] = 11'b001_0000_1000;
      test_symbols[27] = 11'b010_0001_0000;
      test_symbols[28] = 11'b000_0100_0000; //Game 4 done P2 Lose
      test_symbols[29] = 11'b001_0000_1000;
      test_symbols[30] = 11'b110_0001_0000;
      test_symbols[31] = 11'b000_0010_0000; //Game 5 done P1 Win 
      test_symbols[32] = 11'b001_0000_1000;
      test_symbols[33] = 11'b010_0001_0000;
      test_symbols[34] = 11'b000_0100_1000; //Game 6 done P2 Lose
      test_symbols[35] = 11'b000_0000_0100;
      test_symbols[36] = 11'b000_1000_0100; //Back to NOPL
   end
   
   nonsynth_clock_gen
     #(.cycle_time_p(10))
   cg
     (.clk_o(clk_i));

   nonsynth_reset_gen
     #(.num_clocks_p(1)
       ,.reset_cycles_lo_p(1)
       ,.reset_cycles_hi_p(10))
   rg
     (.clk_i(clk_i)
      ,.async_reset_o(reset_i));

  play_states
     #()
   dut
     (.clk_i(clk_i)
     ,.reset_i(reset_i | reset_button)
     ,.in_play_i(in_play_i)
     ,.five_sec_i(five_sec_i)
     ,.ten_sec_i(ten_sec_i)
     ,.win_i(win_i)
     ,.quit_i(reset_button)
     ,.nopl_o(nopl_o)
     ,.flas_o(flas_o)
     ,.play_o(play_o)
     ,.wins_o(wins_o)
     ,.lose_o(lose_o));

   initial begin
      reset_button = 0;      
`ifdef VERILATOR
      $dumpfile("verilator.fst");
`else
      $dumpfile("iverilog.vcd");
`endif
      $dumpvars;

      $display();
      $display("  ______          __  __                    __      ");
      $display(" /_  __/__  _____/ /_/ /_  ___  ____  _____/ /_     ");
      $display("  / / / _ \\/ ___/ __/ __ \\/ _ \\/ __ \\/ ___/ __ \\");
      $display(" / / /  __(__  ) /_/ /_/ /  __/ / / / /__/ / / /  / /");
      $display("/_/  \\___/____/\\__/_.___/\\___/_/ /_/\\___/_/ /_/  /_/");
                                                                                        
      $display();
      $display("Begin Test:");

      itervar = 0;
      {win_i, ten_sec_i, five_sec_i, in_play_i, correct_lose_o, correct_win_o, correct_play_o, correct_flas_o, correct_nopl_o, reset_button, no_input} = test_symbols[itervar];

      @(negedge reset_i);

      reset_done = 1'b1;
      correct_lose_o = 1'b0;
      correct_win_o = 1'b0;
      correct_play_o = 1'b0;
      correct_flas_o = 1'b0;
      correct_nopl_o = 1'b0;

      for(itervar = 0; itervar <= iterations_lp; itervar ++) begin
         correct_lose_o = test_symbols[itervar][6];
      	 correct_win_o = test_symbols[itervar][5];
         correct_play_o = test_symbols[itervar][4];
         correct_flas_o = test_symbols[itervar][3];
         correct_nopl_o = test_symbols[itervar][2];
	 @(posedge clk_i);
	 $display("At Posedge %d: W: %b Ten_sec: %b, Five_sec: %b, In_play: %b, R:%b reset_i = %b ", itervar, win_i, ten_sec_i, five_sec_i, in_play_i, reset_button, reset_i);
      end
      
   end

   assign error_nopl_o = (correct_nopl_o !== nopl_o) && (itervar < iterations_lp);
   assign error_flas_o = (correct_flas_o !== flas_o) && (itervar < iterations_lp);
   assign error_play_o = (correct_play_o !== play_o) && (itervar < iterations_lp);
   assign error_win_o = (correct_win_o !== win_o) && (itervar < iterations_lp);
   assign error_lose_o = (correct_lose_o !== lose_o) && (itervar < iterations_lp);
   
   always @(negedge clk_i) begin
      // {Win, Ten-Sec, Five-Sec, In-Play, LOSE, WINS, PLAY, FLAS, NOPL, Reset, No Input}
      {win_i, ten_sec_i, five_sec_i, in_play_i, correct_lose_n, correct_win_n, correct_play_n, correct_flas_n, correct_nopl_n, reset_button, no_input} = test_symbols[itervar];
      if(!reset_i && (reset_done == 1) && (error_nopl_o == 1)) begin
         $display("\033[0;31mError!\033[0m: nopl_o should be %b, but got %b", correct_nopl_o, nopl_o);
         $finish();
      end
      if(!reset_i && (reset_done == 1) && (error_flas_o == 1)) begin
         $display("\033[0;31mError!\033[0m: flas_o should be %b, but got %b", correct_flas_o, flas_o);
         $finish();
      end
      if(!reset_i && (reset_done == 1) && (error_play_o == 1)) begin
         $display("\033[0;31mError!\033[0m: play_o should be %b, but got %b", correct_play_o, play_o);
         $finish();
      end
      if(!reset_i && (reset_done == 1) && (error_win_o == 1)) begin
         $display("\033[0;31mError!\033[0m: win_o should be %b, but got %b", correct_win_o, win_o);
         $finish();
      end
      if(!reset_i && (reset_done == 1) && (error_lose_o == 1)) begin
         $display("\033[0;31mError!\033[0m: lose_o should be %b, but got %b", correct_lose_o, lose_o);
         $finish();
      end
      if (itervar >= iterations_lp)
        $finish();
   end

   final begin
      $display("Simulation time is %t", $time);
      if(error_nopl_o || error_flas_o || error_play_o || error_win_o || error_lose_o) begin
	 $display("\033[0;31m    ______                    \033[0m");
	 $display("\033[0;31m   / ____/_____________  _____\033[0m");
	 $display("\033[0;31m  / __/ / ___/ ___/ __ \\/ ___/\033[0m");
	 $display("\033[0;31m / /___/ /  / /  / /_/ / /    \033[0m");
	 $display("\033[0;31m/_____/_/  /_/   \\____/_/     \033[0m");
	 $display();
	 $display("Simulation Failed");
      end else begin
	 $display("\033[0;32m    ____  ___   __________\033[0m");
	 $display("\033[0;32m   / __ \\/   | / ___/ ___/\033[0m");
	 $display("\033[0;32m  / /_/ / /| | \\__ \\\__ \ \033[0m");
	 $display("\033[0;32m / ____/ ___ |___/ /__/ / \033[0m");
	 $display("\033[0;32m/_/   /_/  |_/____/____/  \033[0m");
	 $display();
	 $display("Simulation Succeeded!");
      end // else: !if(error_unlock_o)
   end

endmodule
