module game_states
   (input [0:0] clk_i
   ,input [0:0] reset_i
   ,input [0:0] start_i
   ,input [0:0] three_played_i
   ,input [0:0] w_i
   ,input [0:0] quit_i
   ,output [0:0] idle_o
   ,output [0:0] play_o
   ,output [0:0] win_o
   ,output [0:0] no_win_o
   );
   
   //State machine for the entire reaction game
         
   enum logic [1:0] {IDLE = 2'b00,
                     PLAY = 2'b01,
                     WINS = 2'b10,
                     NOWS = 2'b11
                     }
                     state_r;
   
   logic [1:0] curr_state_l;
   
   logic [0:0] idle_l;
   logic [0:0] play_l;
   logic [0:0] win_l;
   logic [0:0] no_win_l;
   
   always_ff @(posedge clk_i) begin
   	if (reset_i | quit_i) begin
   		curr_state_l <= IDLE;
   		idle_l <= 1'b1;
   		play_l <= 1'b0;
   		win_l <= 1'b0;
   		no_win_l <= 1'b0;
   	end else begin
   		case(curr_state_l)
   			IDLE: 
   				begin
   					if(start_i) begin
   						curr_state_l <= PLAY;
   						idle_l <= 1'b0;
   						play_l <= 1'b1;  
   					end else begin
   						curr_state_l <= IDLE;
   						idle_l <= 1'b1;
   						play_l <= 1'b0;
   						win_l <= 1'b0;
   						no_win_l <= 1'b0;
   					end
   				end
   			PLAY:
   				begin
   					if(three_played_i) begin
   						if( w_i ) begin
   							curr_state_l <= WINS;
   							play_l <= 1'b0;
   							win_l <= 1'b1;
   							no_win_l <= 1'b0;
   						end else begin
   							curr_state_l <= NOWS;
   							play_l <= 1'b0;
   							win_l <= 1'b0;
   							no_win_l <= 1'b1;
   						end
   					end else begin
   						curr_state_l <= PLAY;
				   		idle_l <= 1'b0;
				   		play_l <= 1'b1;
				   		win_l <= 1'b0;
				   		no_win_l <= 1'b0;
   					end
   				end
   			WINS:
   				begin
   					curr_state_l <= IDLE;
   					idle_l <= 1'b1;
   					win_l <= 1'b0;
   				end
   			NOWS:
   				begin
   					curr_state_l <= IDLE;
   					idle_l <= 1'b1;
   					no_win_l <= 1'b0;
   				end
   			default: 
   				begin
   					curr_state_l <= IDLE;
   					idle_l <= 1'b1;
   					play_l <= 1'b0;
   					win_l <= 1'b0;
   					no_win_l <= 1'b0;
   				end 
   		endcase
   	end
   end
   
   assign idle_o = idle_l;
   assign play_o = play_l;
   assign win_o = win_l;
   assign no_win_o = no_win_l;
   
endmodule
