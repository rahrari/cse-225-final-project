module play_states
   (input [0:0] clk_i
   ,input [0:0] reset_i
   ,input [0:0] in_play_i
   ,input [0:0] five_sec_i
   ,input [0:0] ten_sec_i
   ,input [0:0] win_i
   ,input [0:0] quit_i
   ,output [0:0] nopl_o
   ,output [0:0] flas_o
   ,output [0:0] play_o
   ,output [0:0] wins_o
   ,output [0:0] lose_o
   );
   
   //State machine for rounds
         
   enum logic [2:0] {NOPL = 3'b000,
                     FLAS = 3'b001,
                     PLAY = 3'b010,
                     WINS = 3'b011,
                     LOSE = 3'b100
                     }
                     state_r;
   
   logic [2:0] curr_state_l;
   
   logic [0:0] nopl_l;
   logic [0:0] flas_l;
   logic [0:0] play_l;
   logic [0:0] wins_l;
   logic [0:0] lose_l;
   
   logic [2:0] games_l; 
   
   always_ff @(posedge clk_i) begin
   	if (reset_i | quit_i) begin
   		curr_state_l <= NOPL;
   		nopl_l <= 1'b1;
   		flas_l <= 1'b0;
   		play_l <= 1'b0;
   		wins_l <= 1'b0;
   		lose_l <= 1'b0;
   		games_l <= 3'b101;
   	end else begin
   		case(curr_state_l)
   			NOPL: 
   				begin
   					if(in_play_i) begin
   						curr_state_l <= FLAS;
   						nopl_l <= 1'b0;
   						flas_l <= 1'b1;  
   					end else begin 
   						curr_state_l <= NOPL;
				   		nopl_l <= 1'b1;
				   		flas_l <= 1'b0;
				   		play_l <= 1'b0;
				   		wins_l <= 1'b0;
				   		lose_l <= 1'b0;
   					end
   				end
   			FLAS:
   				begin
   					if(five_sec_i) begin
   						curr_state_l <= PLAY;
   						play_l <= 1'b1;
   						flas_l <= 1'b0;
	     				end else begin
	     					curr_state_l <= FLAS;
				   		nopl_l <= 1'b0;
				   		flas_l <= 1'b1;
				   		play_l <= 1'b0;
				   		wins_l <= 1'b0;
				   		lose_l <= 1'b0;
					end
   				end
   			PLAY:
   				begin	
   					if (win_i) begin
   						curr_state_l <= WINS;
   						play_l <= 1'b0;
   						wins_l <= 1'b1;	
   					end else begin
   						if(ten_sec_i) begin
   							curr_state_l <= LOSE;
   							play_l <= 1'b0;
 							lose_l <= 1'b1;	
   						end else begin
   							curr_state_l <= PLAY;
   						end
   					end
   				end
   			WINS:
   				begin
   					if(games_l == '0) begin
   						curr_state_l <= NOPL;
   						nopl_l <= 1'b1;
					   	flas_l <= 1'b0;
					   	play_l <= 1'b0;
					   	wins_l <= 1'b0;
					   	lose_l <= 1'b0;
					   	games_l <= 3'b101;
	   				end else begin
	   					curr_state_l <= FLAS;
	   					games_l <= (games_l + '1);
	   					wins_l <= 1'b0;
	   					flas_l <= 1'b1;  
   					end
   				end
   			LOSE:
   				begin
   					if(games_l == '0) begin
   						curr_state_l <= NOPL;
   						nopl_l <= 1'b1;
					   	flas_l <= 1'b0;
					   	play_l <= 1'b0;
					   	wins_l <= 1'b0;
					   	lose_l <= 1'b0;
					   	games_l <= 3'b101;
	   				end else begin
	   					curr_state_l <= FLAS;
	   					games_l <= (games_l + '1);
	   					lose_l <= 1'b0;
	   					flas_l <= 1'b1; 
	   				end
   				end
   			default: 
   				begin
   					curr_state_l <= NOPL;
   					nopl_l <= 1'b1;
   					flas_l <= 1'b0;
   					play_l <= 1'b0;
   					wins_l <= 1'b0;
   					lose_l <= 1'b0;
   					games_l <= 3'b101;
   				end 
   		endcase
   	end
   end
   
   assign nopl_o = nopl_l;
   assign flas_o = flas_l;
   assign play_o = play_l;
   assign wins_o = wins_l;
   assign lose_o = lose_l;
   
endmodule
