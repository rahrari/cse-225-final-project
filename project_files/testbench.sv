// Ryan Ahrari Final Proj Testbench file

`timescale 1ns/1ps
module testbench();
   localparam iterations_lp = 73;
   logic [0:0] reset_done = 1'b0;

   wire [0:0]  clk_i;
   wire [0:0]  reset_i;
   logic [0:0] reset_button; //quit_button_i input would be an actual button on keypad if I had finished
   logic [0:0] start_i;
   logic [0:0] p1_press_i;
   logic [0:0] p2_press_i;
   logic [0:0] five_sec_i;
   logic [0:0] ten_sec_i;
   logic [0:0] no_input;
   wire [0:0]  p1_wins_o;
   wire [0:0]  p2_wins_o;

   wire [0:0]  error_p1_wins_o;
   logic [0:0] correct_p1_wins_o;
   logic [0:0] correct_p1_wins_n;
   
   wire [0:0]  error_p2_wins_o;
   logic [0:0] correct_p2_wins_o;
   logic [0:0] correct_p2_wins_n;

   int         itervar;
   logic [8:0] test_symbols [74];

   initial begin
      // {ten_sec_i, five_sec_i, p2_press_i, p1_press_i, start_i, p2_wins_o, p1_wins_o, reset_i, no_input}
      // Template: test_symbols[ x] = 9'b0_0000_0000;
      
      test_symbols[ 0] = 9'b0_0000_0001;
      test_symbols[ 1] = 9'b0_0001_0000;
      test_symbols[ 2] = 9'b0_0000_0000; 	//GS: In-Play, PS: No-Play	//targ <= lfsr	
      test_symbols[ 3] = 9'b0_1000_0000;	//GS: In-Play, PS: Flash	//P1 Turn Starts
      test_symbols[ 4] = 9'b0_0010_0000;	//GS: In-Play, PS: Wins 	//Games -= 1 	//targ <= lfsr
      test_symbols[ 5] = 9'b0_0000_0000;	//GS: In-Play, PS: Flash	//P2 Turn Starts
      test_symbols[ 6] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[ 7] = 9'b0_1000_0000;	//GS: In-Play, PS: Playing
      test_symbols[ 8] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[ 9] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[10] = 9'b0_0000_0000;    	//GS: In-Play, PS: Playing	
      test_symbols[11] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing 	
      test_symbols[12] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[13] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[14] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing         
      test_symbols[15] = 9'b0_0100_0000;	//GS: In-Play, PS: Wins		//Games -= 1 	//targ <= lfsr
      test_symbols[16] = 9'b0_0000_0000;	//GS: In-Play, PS: Flash
      test_symbols[17] = 9'b0_0000_0000;	//GS: In-Play, PS: Flash
      test_symbols[18] = 9'b0_0000_0000;    	//GS: In-Play, PS: Flash 	
      test_symbols[19] = 9'b0_1000_0000;	//GS: In-Play, PS: Playing	//P1 Turn Starts
      test_symbols[20] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[21] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[22] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[23] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[24] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[25] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[26] = 9'b0_0000_0000;    	//GS: In-Play, PS: Playing	
      test_symbols[27] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing 	
      test_symbols[28] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[29] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[30] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[31] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[32] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[33] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[34] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[35] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[36] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[37] = 9'b0_0010_0000;	//GS: In-Play, PS: Wins		//Games -= 1 	//targ <= lfsr
      test_symbols[38] = 9'b0_0000_0000;	//GS: In-Play, PS: Flash	
      test_symbols[39] = 9'b0_0000_0000;	//GS: In-Play, PS: Flash
      test_symbols[40] = 9'b0_0000_0000;	//GS: In-Play, PS: Flash
      test_symbols[41] = 9'b0_0000_0000;    	//GS: In-Play, PS: Flash 	
      test_symbols[42] = 9'b0_1000_0000;	//GS: In-Play, PS: Playing 	//P2 Turn Starts
      test_symbols[43] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[44] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[45] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[46] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[47] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[48] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[49] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[50] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[51] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[52] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[53] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[54] = 9'b0_0100_0000;	//GS: In-Play, PS: Wins		//Games -= 1 	//targ <= lfsr
      test_symbols[55] = 9'b0_0000_0000;	//GS: In-Play, PS: Flash
      test_symbols[56] = 9'b0_0000_0000;	//GS: In-Play, PS: Flash
      test_symbols[57] = 9'b0_1000_0000;	//GS: In-Play, PS: Playing	//P1 Turn Starts
      test_symbols[58] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[59] = 9'b0_0010_0000;	//GS: In-Play, PS: Wins		//Games -= 1 	//targ <= lfsr
      test_symbols[60] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[61] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[62] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[63] = 9'b0_1000_0000;	//GS: In-Play, PS: Playing   	//P2 Turn Starts
      test_symbols[64] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[65] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[66] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[67] = 9'b1_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[68] = 9'b0_0000_0000;	//TOP: P1 Wins
      test_symbols[69] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[70] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[71] = 9'b0_0000_0100;	//GS: In-Play, PS: Playing
      test_symbols[72] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
      test_symbols[73] = 9'b0_0000_0000;	//GS: In-Play, PS: Playing
   end
   
   nonsynth_clock_gen
     #(.cycle_time_p(10))
   cg
     (.clk_o(clk_i));

   nonsynth_reset_gen
     #(.num_clocks_p(1)
       ,.reset_cycles_lo_p(1)
       ,.reset_cycles_hi_p(10))
   rg
     (.clk_i(clk_i)
      ,.async_reset_o(reset_i));

  top
     #()
   dut
     (.clk_i(clk_i)
     ,.reset_i(reset_i)
     ,.quit_button_i(reset_button)
     ,.start_i(start_i)
     ,.p1_press_i(p1_press_i)
     ,.p2_press_i(p2_press_i)
     ,.five_sec_i(five_sec_i)
     ,.ten_sec_i(ten_sec_i)
     ,.p1_wins_o(p1_wins_o)
     ,.p2_wins_o(p2_wins_o));

   initial begin
      reset_button = 0;      
`ifdef VERILATOR
      $dumpfile("verilator.fst");
`else
      $dumpfile("iverilog.vcd");
`endif
      $dumpvars;

      $display();
      $display("  ______          __  __                    __      ");
      $display(" /_  __/__  _____/ /_/ /_  ___  ____  _____/ /_     ");
      $display("  / / / _ \\/ ___/ __/ __ \\/ _ \\/ __ \\/ ___/ __ \\");
      $display(" / / /  __(__  ) /_/ /_/ /  __/ / / / /__/ / / /  / /");
      $display("/_/  \\___/____/\\__/_.___/\\___/_/ /_/\\___/_/ /_/  /_/");
                                                                                        
      $display();
      $display("Begin Test:");

      itervar = 0;
      {ten_sec_i, five_sec_i, p2_press_i, p1_press_i, start_i, correct_p2_wins_o, correct_p1_wins_o, reset_button, no_input} = test_symbols[itervar];

      @(negedge reset_i);

      reset_done = 1'b1;
      correct_p2_wins_o = 1'b0;
      correct_p1_wins_o = 1'b0;

      for(itervar = 0; itervar <= iterations_lp; itervar ++) begin
         correct_p2_wins_o = test_symbols[itervar][3];
         correct_p1_wins_o = test_symbols[itervar][2];
	 @(posedge clk_i);
	 $display("At Posedge %d: Ten_sec: %b, Five_sec: %b, P1_press: %b, P2_press: %b,  Start_i: %b, R:%b reset_i = %b ", itervar, ten_sec_i, five_sec_i, p1_press_i, p2_press_i, start_i, reset_button, reset_i);
      end
      
   end

   assign error_p1_wins_o = (correct_p1_wins_o !== p1_wins_o) && (itervar < iterations_lp);
   assign error_p2_wins_o = (correct_p2_wins_o !== p2_wins_o) && (itervar < iterations_lp);
   
   always @(negedge clk_i) begin
      // {Win, Ten-Sec, Five-Sec, In-Play, LOSE, WINS, PLAY, FLAS, NOPL, Reset, No Input}
       {ten_sec_i, five_sec_i, p2_press_i, p1_press_i, start_i, correct_p2_wins_n, correct_p1_wins_n, reset_button, no_input} = test_symbols[itervar];
      if(!reset_i && (reset_done == 1) && (error_p1_wins_o == 1)) begin
         $display("\033[0;31mError!\033[0m: p1_wins_o should be %b, but got %b", correct_p1_wins_o, p1_wins_o);
         //$finish();
      end
      if(!reset_i && (reset_done == 1) && (error_p2_wins_o == 1)) begin
         $display("\033[0;31mError!\033[0m: p2_wins_o should be %b, but got %b", correct_p2_wins_o, p2_wins_o);
         //$finish();
      end
      if (itervar >= iterations_lp)
        $finish();
   end

   final begin
      $display("Simulation time is %t", $time);
      if(error_p1_wins_o || error_p2_wins_o) begin
	 $display("\033[0;31m    ______                    \033[0m");
	 $display("\033[0;31m   / ____/_____________  _____\033[0m");
	 $display("\033[0;31m  / __/ / ___/ ___/ __ \\/ ___/\033[0m");
	 $display("\033[0;31m / /___/ /  / /  / /_/ / /    \033[0m");
	 $display("\033[0;31m/_____/_/  /_/   \\____/_/     \033[0m");
	 $display();
	 $display("Simulation Failed");
      end else begin
	 $display("\033[0;32m    ____  ___   __________\033[0m");
	 $display("\033[0;32m   / __ \\/   | / ___/ ___/\033[0m");
	 $display("\033[0;32m  / /_/ / /| | \\__ \\\__ \ \033[0m");
	 $display("\033[0;32m / ____/ ___ |___/ /__/ / \033[0m");
	 $display("\033[0;32m/_/   /_/  |_/____/____/  \033[0m");
	 $display();
	 $display("Simulation Succeeded!");
      end // else: !if(error_unlock_o)
   end

endmodule
